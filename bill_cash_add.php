<html>
<head>
	<?php
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>เพิ่มบิลภาษีเงินสด</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">เพิ่มบิลภาษีเงินสด</p></h2>
			<div class="margin-lr box center">
				<form action="bill_cash_add_process.php" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								$year = date("Y")+543;
								// ID
								$yearID = date("y")+43;
								if ($yearID > 100) {
									$yearID -= 100;
								}
								$id = "CR".$yearID."-";
								for ($i=0; $i < 4-strlen($_SESSION['ID']+1); $i++) { 
									$id .= "0";
								}
								$id .= $_SESSION['ID']+1;
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td>
													เลขที่บิล
												</td>
												<td>
													'.$id.'<input name="CashCode" type="text" value="'.($_SESSION['ID']+1).'"/>
												</td>
											</tr>
											<tr>
												<td style="width:90px">วันที่</td>
												<td colspan="2"><input name="CashDate" type="text" value="'.date("d-m-").$year.'"/></td>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px">ค้นหาชื่อ</td>
												<td>
													<input class="name" name="CashCustomerName" type="text" onkeyup="SearchWord(this.value)"/> 
													<a class="btn margin-lr smooth" href="customer_add.php?page=bill_cash_add">เพิ่มลูกค้า</a>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="NameList">';
								include('namelist.php');
								echo				'</span>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="CustomerDetail">
														<table>
															<tr>
																<td>ที่อยู่</td>
															</tr>
															<tr>
																<td>เบอร์</td>
															</tr>
															<tr>
																<td>Fax</td>
															</tr>
															<tr>
																<td>สำนักงาน</td>
															</tr>
															<tr>
																<td>เลขประจำตัวผู้เสียภาษี</td>
															</tr>
														</table>
													</span>
												</td>
											</tr>
										</table>
										<table class="noborder margin-b list" style="width:auto">
											<tr>
												<td colspan="5"><b style="font-size:1.4em;"><a name="productList">รายการสินค้า</a></b></td>
											</tr>
											<tr>
												<td class="text-cen">เลขที่</td>
												<td class="text-cen">ชื่อ</td>
												<td class="text-cen">จำนวน</td>
												<td class="text-cen">หน่วย</td>
												<td class="text-cen">ราคาต่อหน่วย</td>
												<td style="width:100px" class="text-cen">รวม</td>
											</tr>
											<tr>
												<td>1</td>
												<td><input class="name" name="ProductName1" type="text" /></td>
												<td><input name="ProductQuantity1" id="ProductQuantity1" type="text" value="0" onkeyup="CalPrice(\'1\')"/></td>
												<td><input name="ProductUnit1" type="text" /></td>
												<td><input name="ProductPriceEach1" id="ProductPriceEach1" type="text" value="0" onkeyup="CalPrice(\'1\')"/></td>
												<td class="text-right">
													<span id="ProductTotalText1">0.00</span>
													<input type="hidden" id="ProductTotal1" name="ProductTotal1" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="4"><b style="font-size:1.2em;">รวม</b></td>
												<td></td>
												<td class="text-right">
													<span id="ProductAllTotalText">0.00</span>
													<input type="hidden" id="ProductAllTotal" name="ProductAllTotal" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="4"><b style="font-size:1.2em;">ภาษี</b></td>
												<td>
													<input name="CashVat" id="CreditVat" type="text" size="5" value="7" onkeyup="CalPrice(\'0\')"/> % 
												</td>
												<td class="text-right">
													<span id="VatText">0.00</span>
													<input type="hidden" id="Vat" name="Vat" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="4"><b style="font-size:1.5em;">รวมทั้งสิ้น</b></td>
												<td></td>
												<td class="text-right">
													<span id="ProductAllTotalAddVatText">0.00</span>
													<input type="hidden" id="ProductAllTotalAddVat" name="ProductAllTotalAddVat" value="0" />
												</td>
											</tr>
										</table>
										<table class="noborder margin-b" style="width:auto">
											<tr>
												<td width="220px">
												</td>
												<td>
													<a class="btn smooth del-etc-row float-r">-</a>
													<div class="float-r">&nbsp;</div>
													<a class="btn smooth add-etc-row float-r">+</a>
												</td>
											</tr>
										</table>';
							?>
						</div>
					</div>
					<input class="btn smooth" type="submit" value="เพิ่มบิล"></a>
					<a class="btn smooth" href="bill_cash.php">กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>