<html>
<head>
	<?php
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>การเพิ่มบิลภาษีเครดิต</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">การเพิ่มบิลภาษีเครดิต</p></h2>
			<div class="margin-lr box center">
				<form action="bill_credit_add_process.php" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								$TH_Month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
								$year = date("Y")+543;
								// ID
								$yearID = date("y")+43;
								if ($yearID > 100) {
									$yearID -= 100;
								}
								$id = "IV".$yearID."-";
								for ($i=0; $i < 4-strlen($_SESSION['ID']+1); $i++) { 
									$id .= "0";
								}
								$id .= $_SESSION['ID']+1;

								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td>
													เลขที่บิล
												</td>
												<td>
													'.$id.'<input name="CreditCode" type="hidden" value="'.($_SESSION['ID']+1).'"/>
												</td>
											</tr>
											<tr>
												<td style="width:90px">วันที่</td>
												<td>
													<select id="day" name="day" onchange="CalDeadLine(this.value,\'day\')">';
								$isSelected = false;
								for ($i=1; $i <= 31 ; $i++) { 
									echo '<option value="'.$i.'" ';
									if (!$isSelected) {
										if ($i == date("d")) {
											echo 'selected';
											$isSelected = true;
										}
									}
									echo'>'.$i.'</option>';
								}
								echo				'</select>
													<select id="month" name="month" onchange="CalDeadLine(this.value,\'month\')">';
								$isSelected = false;
								for ($i=1; $i <= 12 ; $i++) { 
									echo '<option value="'.$i.'"';
									if (!$isSelected) {
										if ($i == date("m")) {
											echo 'selected';
											$isSelected = true;
										}
									}
									echo '>'.$i.'</option>';
								}
								echo				'
													</select>
													'.(date("Y")+543).'<input type="hidden" id="year" name="year" value="'.date("Y").'" />
													</select>
												</td>
											</tr>
											<tr>
												<td>กำหนดชำระ</td>
												<td colspan="2">
													<span id="Deadline">';
								include('Deadline.php');
								echo 				'</span>
												</td>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px">ค้นหาชื่อ</td>
												<td>
													<input class="name" name="CreditCustomerName" type="text" onkeyup="SearchWord(this.value)"/> 
													<a class="btn margin-lr smooth" href="customer_add.php?page=bill_credit_add">เพิ่มลูกค้า</a>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="NameList">';
								include('namelist.php');
								echo				'</span>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="CustomerDetail">
														<table>
															<tr>
																<td>ที่อยู่</td>
															</tr>
															<tr>
																<td>เบอร์</td>
															</tr>
															<tr>
																<td>Fax</td>
															</tr>
															<tr>
																<td>สำนักงาน</td>
															</tr>
															<tr>
																<td>เลขประจำตัวผู้เสียภาษี</td>
															</tr>
														</table>
													</span>
												</td>
											</tr>
											<tr>
												<td>เลขที่ใบสั่งซื้อ</td>
												<td colspan="2"><input name="CreditPO" type="text" /></td>
											</tr>
										</table>
										<table class="noborder margin-b list" style="width:auto">
											<tr>
												<td colspan="5"><b style="font-size:1.4em;">รายการสินค้า</b></td>
											</tr>
											<tr>
												<td class="text-cen" style="height:50px">เลขที่</td>
												<td class="text-cen">ชื่อ</td>
												<td class="text-cen">จำนวน</td>
												<td class="text-cen">หน่วย</td>
												<td class="text-cen">ราคาต่อหน่วย</td>
												<td style="width: 100px" class="text-cen">รวม</td>
											</tr>
											<tr>
												<td>1</td>
												<td><input class="name" name="ProductName1" type="text" /></td>
												<td><input name="ProductQuantity1" id="ProductQuantity1" type="text" value="0" onkeyup="CalPrice(\'1\')"/></td>
												<td><input name="ProductUnit1" type="text" /></td>
												<td><input name="ProductPriceEach1" id="ProductPriceEach1" type="text" value="0" onkeyup="CalPrice(\'1\')"/></td>
												<td class="text-right">
													<span id="ProductTotalText1">0.00</span>
													<input type="hidden" id="ProductTotal1" name="ProductTotal1" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="4"><b style="font-size:1.2em;">รวม</b></td>
												<td></td>
												<td class="text-right">
													<span id="ProductAllTotalText">0.00</span>
													<input type="hidden" id="ProductAllTotal" name="ProductAllTotal" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="4"><b style="font-size:1.2em;">ภาษี</b></td>
												<td>
													<input name="CreditVat" id="CreditVat" type="text" size="5" value="7" onkeyup="CalPrice(\'0\')"/> % 
												</td>
												<td class="text-right">
													<span id="VatText">0.00</span>
													<input type="hidden" id="Vat" name="Vat" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="4"><b style="font-size:1.5em;">รวมทั้งสิ้น</b></td>
												<td></td>
												<td class="text-right">
													<span id="ProductAllTotalAddVatText">0.00</span>
													<input type="hidden" id="ProductAllTotalAddVat" name="ProductAllTotalAddVat" value="0" />
												</td>
											</tr>
										</table>
										<table class="noborder" style="width:auto">
											<tr>
												<td width="220px">
												</td>
												<td>
													<a class="btn smooth del-etc-row float-r">-</a>
													<div class="float-r">&nbsp;</div>
													<a class="btn smooth add-etc-row float-r">+</a>
												</td>
											</tr>
										</table>';
							?>
						</div>
					</div>
					<input class="btn smooth" type="submit" value="เพิ่มบิล"></a>
					<a class="btn smooth" href="index.php">กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>