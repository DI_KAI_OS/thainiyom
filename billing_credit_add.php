<html>
<head>
	<?php
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>การเพิ่มใบวางบิลภาษีเครดิต</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">การเพิ่มใบวางบิลภาษีเครดิต</p></h2>
			<div class="margin-lr box center">
				<form action="billing_credit_add_process.php" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								if (!isset($_SESSION['dayStart'])) {
									$_SESSION['dayStart'] = date("d");
								}
								if (!isset($_SESSION['monthStart'])) {
									$_SESSION['monthStart'] = date("m",strtotime("-1 month"));
								}
								if (!isset($_SESSION['yearStart'])) {
									$_SESSION['yearStart'] = date("Y",strtotime("-1 month"));
								}
								if (!isset($_SESSION['dayEnd'])) {
									$_SESSION['dayEnd'] = date("d");
								}
								if (!isset($_SESSION['monthEnd'])) {
									$_SESSION['monthEnd'] = date("m");
								}
								if (!isset($_SESSION['yearEnd'])) {
									$_SESSION['yearEnd'] = date("Y");
								}
								if (!isset($_SESSION['customer'])) {
									$_SESSION['customer'] = 1;
								}

								$TH_Month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
								$year = date("Y")+543;
								// ID
								$sql = "SELECT * FROM BillingCredit";
								if ($result = mysqli_query($conn, $sql)) {
									$max_code = 0;
									while ($rowBillingCredit = mysqli_fetch_assoc($result)) {
										if (date("Y", strtotime($rowBillingCredit['DateEnd'])) == date("Y") && $max_code < $rowBillingCredit['Code']) {
											$max_code = $rowBillingCredit['Code'];
										}
									}
								}
								$next_code = $max_code+1;
								$id = ($next_code);
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td>
													เลขที่
												</td>
												<td>
													'.$id.'<input name="Code" type="hidden" value="'.$id.'"/>
												</td>
											</tr>
											<tr>
												<td style="width:90px">วันที่</td>
												<td style="text-align:right;">
													เริ่ม
												</td>
												<td>
													<select id="dayStart" name="dayStart" onchange="SelectBillingCreditByCustomer(this.value,\'dayStart\')">';
								$isSelected = false;
								for ($i=1; $i <= 31 ; $i++) {
									echo '<option value="'.$i.'" ';
									if (!$isSelected) {
										if (isset($_SESSION['dayStart']) AND $i == $_SESSION['dayStart']) {
											echo 'selected';
											$isSelected = true;
										}elseif ($i == date("d")) {
											echo 'selected';
											$isSelected = true;
										}
									}
									echo'>'.$i.'</option>';
								}
								echo				'</select>
													<select id="monthStart" name="monthStart" onchange="SelectBillingCreditByCustomer(this.value,\'monthStart\')">';
								$isSelected = false;
								for ($i=1; $i <= 12 ; $i++) {
									echo '<option value="'.$i.'"';
									if (!$isSelected) {
										if (isset($_SESSION['monthStart']) AND $i == $_SESSION['monthStart']) {
											echo 'selected';
											$isSelected = true;
										}elseif ($i == date("m",strtotime("-1 month"))) {
											echo 'selected';
											$isSelected = true;
										}
									}
									echo '>'.$TH_Month[$i-1].'</option>';
								}
								echo				'
													</select>
													<select id="yearStart" name="yearStart" onchange="SelectBillingCreditByCustomer(this.value,\'yearStart\')">';
								$isFirst = true;
								for ($i=0; $i < 5 ; $i++) {
									// check is January
									if (date("m") == 1 && $isFirst) {
										$isFirst = false;
										echo '<option value="'.(date("Y",strtotime("-1 month +1 year"))).'"';
										if (isset($_SESSION['yearStart']) AND date("Y",strtotime("-1 month +1 year")) == $_SESSION['yearStart']) {
											echo 'selected';
										}
										echo '>'.(date("Y",strtotime("-1 month"))+1+543).'</option>';
										$i--;
									}else{
										echo '<option value="'.(date("Y",strtotime("-1 month -".$i." year"))).'"';
										if (isset($_SESSION['yearStart']) AND date("Y",strtotime("-1 month -".$i." year")) == $_SESSION['yearStart']) {
											echo 'selected';
										}
										echo '>'.(date("Y",strtotime("-1 month"))-$i+543).'</option>';
									}
								}
								echo				'
													</select>
												</td>
											</tr>
											<tr>
												<td></td>
												<td style="text-align:right;">
													ถึง
												</td>
												<td>
													<select id="dayEnd" name="dayEnd" onchange="SelectBillingCreditByCustomer(this.value,\'dayEnd\')">';
								$isSelected = false;
								for ($i=1; $i <= 31 ; $i++) {
									echo '<option value="'.$i.'" ';
									if (!$isSelected) {
										if (isset($_SESSION['dayEnd']) AND $i == $_SESSION['dayEnd']) {
											echo 'selected';
											$isSelected = true;
										}elseif ($i == date("d")) {
											echo 'selected';
											$isSelected = true;
										}
									}
									echo'>'.$i.'</option>';
								}
								echo				'</select>
													<select id="monthEnd" name="monthEnd" onchange="SelectBillingCreditByCustomer(this.value,\'monthEnd\')">';
								$isSelected = false;
								for ($i=1; $i <= 12 ; $i++) {
									echo '<option value="'.$i.'"';
									if (!$isSelected) {
										if (isset($_SESSION['monthEnd']) AND $i == $_SESSION['monthEnd']) {
											echo 'selected';
											$isSelected = true;
										}elseif ($i == date("m")) {
											echo 'selected';
											$isSelected = true;
										}
									}
									echo '>'.$TH_Month[$i-1].'</option>';
								}
								echo				'
													</select>
													<select id="yearEnd" name="yearEnd" onchange="SelectBillingCreditByCustomer(this.value,\'yearEnd\')">';
								for ($i=0; $i < 5 ; $i++) {
									echo '<option value="'.(date("Y")-$i).'"';
									if (isset($_SESSION['yearEnd']) AND date("Y",strtotime("-".$i." year")) == $_SESSION['yearEnd']) {
										echo 'selected';
									}
									echo '>'.($year-$i).'</option>';
								}
								echo				'
													</select>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px">ค้นหาชื่อ</td>
												<td>
													<input class="name" name="CreditCustomerName" type="text" onkeyup="SearchWord(this.value);"/>
													<a class="btn margin-lr smooth" href="customer_add.php?page=bill_credit_add">เพิ่มลูกค้า</a>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="NameList">';
								include('namelist.php');
								echo				'</span>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="CustomerDetail">';
								include('customer_detail.php');
								echo				'</span>
												</td>
											</tr>
										</table>
										<div class="margin-b" id="BillingCreditList">';
								unset($_SESSION['BillList']);
								include('billing_credit_select_by_customer.php');
								echo	'</div>';
								// echo "dayStart:".$_SESSION['dayStart']."<br>";
								// echo "monthStart:".$_SESSION['monthStart']."<br>";
								// echo "yearStart:".$_SESSION['yearStart']."<br>";
								// echo "dayEnd:".$_SESSION['dayEnd']."<br>";
								// echo "monthEnd:".$_SESSION['monthEnd']."<br>";
								// echo "yearEnd:".$_SESSION['yearEnd']."<br>";
								// echo "customer:".$_SESSION['customer']."<br>";
							?>
						</div>
					</div>
					
					<?php $_SESSION['billing_credit_add'] = strtotime(date('Y-m-d H:i:s')); ?>
					<input type="hidden" name="save_token" value="<?=$_SESSION['billing_credit_add']?>">
					<input class="btn smooth" type="submit" value="เพิ่มบิล"></a>
					<a class="btn smooth" href="billing_credit.php">กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>
