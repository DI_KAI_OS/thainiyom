<?php
	session_start();
	include('connection.php');
	if (isset($_GET['type'])) {
		switch ($_GET['type']) {
			case 'dayStart':
				$_SESSION['dayStart'] = $_GET['input'];
				break;
			case 'monthStart':
				$_SESSION['monthStart'] = $_GET['input'];
				break;
			case 'yearStart':
				$_SESSION['yearStart'] = $_GET['input'];
				break;
			case 'dayEnd':
				$_SESSION['dayEnd'] = $_GET['input'];
				break;
			case 'monthEnd':
				$_SESSION['monthEnd'] = $_GET['input'];
				break;
			case 'yearEnd':
				$_SESSION['yearEnd'] = $_GET['input'];
				break;
			case 'customerid':
				$_SESSION['customer'] = $_GET['input'];
				break;

			default:
				# code...
				break;
		}
		// echo "set by type";
	}

	if (isset($_SESSION['dayStart']) && isset($_SESSION['monthStart'])  && isset($_SESSION['yearStart'])) {
		$dateStart = "Date >= '".($_SESSION['yearStart'])."-".$_SESSION['monthStart']."-".$_SESSION['dayStart']."'";
		// echo $dateStart;
	}else{
		unset($dateStart);
	}
	if (isset($_SESSION['dayEnd']) && isset($_SESSION['monthEnd'])  && isset($_SESSION['yearEnd'])) {
		$dateEnd = "Date <= '".($_SESSION['yearEnd'])."-".$_SESSION['monthEnd']."-".$_SESSION['dayEnd']."'";
		// echo $dateEnd;
	}else{
		unset($dateEnd);
	}
	if (isset($_SESSION['customer'])) {
		$custID = "CustomerID='".$_SESSION['customer']."'";
		$_SESSION['CustomerID'] = $_SESSION['customer'];
		// echo $custID;
	}else{
		unset($custID);
		// echo "cusID is not set".$_SESSION['customer'];
	}

	// Bill List
	if (isset($_SESSION['BillList'])) {
		$billList = explode("-", $_SESSION['BillList']);
		$isSetBillList = true;
	}else{
		$isSetBillList = false;
	}
	echo '
		<table>
				<tr>
					<td></td>
					<th width="60px">เลขที่บิล</th>
					<th width="80px">วันที่</th>
					<th>ชื่อลูกค้า</th>
					<th width="100px">เลขที่ใบสั่งซื้อ</th>
					<th>ราคา</th>
					<th>Vat</th>
					<th>ราคารวม</th>
				</tr>';
	$strSQL = "SELECT * FROM VatCredit";
	if (isset($dateStart) OR isset($dateEnd) OR isset($custID)) {
		$strSQL .= " WHERE ";
		if (isset($dateStart)) {
			$strSQL .= $dateStart;
			if (isset($dateEnd)) {
				$strSQL .= " AND ".$dateEnd;
			}
			if (isset($custID)) {
				$strSQL .= " AND ".$custID;
			}
		}else if($dateEnd){
			$strSQL .= $dateEnd;
			if (isset($custID)) {
				$strSQL .= " AND ".$custID;
			}
		}else if($custID){
			$strSQL .= $custID;
		}
	}
	$strSQL .= " ORDER BY Date ASC, ID ASC";
	// echo "<tr>".$strSQL."</tr>";
	$topCode = 0;
	if ($result = mysqli_query($conn,$strSQL)) {
		while($row = mysqli_fetch_assoc($result)){
			if (date("Y",strtotime($row['Date'])) == date("Y")) {
				if ($topCode < $row['Code']) {
					$topCode = $row['Code'];
				}
			}
		}
	}
	$price = 0;
	$vat = 0;
	$total = 0;
	if($result = mysqli_query($conn,$strSQL)){
		$cnt = 1;
		$tmp = "";
		while($row = mysqli_fetch_assoc($result)){
			// ID
			$yearID = (date("y",strtotime($row['Date'])))+43;
			if ($yearID > 100) {
				$yearID -= 100;
			}
			$id = "IV".$yearID."-";
			for ($i=0; $i < 4-strlen($row['Code']); $i++) {
				$id .= "0";
			}
			$id .= $row['Code'];
			// Customer Part
			$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
			if ($cusName != "") {
				$sqlCus .= " AND Name LIKE '%".$cusName."%'";
			}
			if ($resCus = mysqli_query($conn, $sqlCus)) {
				$rowCus = mysqli_fetch_assoc($resCus);
				// Product Part
				$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
				if ($proName != "") {
					$sqlPro .= " AND Name LIKE '%".$proName."%'";
				}
				if ($resPro = mysqli_query($conn, $sqlPro)) {
					$haveProduct = 0;
					while ($rowPro = mysqli_fetch_assoc($resPro)) {
						$haveProduct = 1;
					}
				}
				// if all condition
				if (($rowCus['Name'] != "" OR ($cusName == "" AND $row['CustomerID'] == 0)) AND $haveProduct == 1) {

					$href = "'bill_credit_view.php?creditid=".$row['ID']."'";
					$year = date("Y",strtotime($row['Date']))+543;
					echo '<tr>
							<td><input type="checkbox" name="check'.$cnt.'" id="check'.$cnt.'" ';
					if ($isSetBillList) {
						for ($j=0; $j < $_SESSION['BillCnt']; $j++) {
							if($row['ID'] == $billList[$j]){
								echo "checked";
								$price += round(($row['Total']/(100+$row['Vat'])*100),2);
								$vat += round(($row['Total']/(100+$row['Vat'])*7),2);
								$total += $row['Total'];
							}
						}
					}else{
						echo "checked";
					}
					echo ' onchange="BillingCreditCal()" value="1"></td>
							<td style="text-align:center;">'.$id.'<input type="hidden" name="id'.$cnt.'" value="'.$row['ID'].'"></td>
							<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
							<td>'.$rowCus['Name'].'</td>
							<td style="text-align:center;">'.$row['PO'].'</td>
							<td style="text-align:right;">'
								.number_format(round(($row['Total']/(100+$row['Vat'])*100),2), 2, '.', ',').$space.$space.'
								<input type="hidden" name="price'.$cnt.'" id="price'.$cnt.'" value="'.round(($row['Total']/(100+$row['Vat'])*100),2).'">
							</td>
							<td style="text-align:right;">'
								.number_format(round(($row['Total']/(100+$row['Vat'])*7),2), 2, '.', ',').$space.$space.'
								<input type="hidden" name="vat'.$cnt.'" id="vat'.$cnt.'" value="'.round(($row['Total']/(100+$row['Vat'])*7),2).'">
							</td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>"
								.number_format($row['Total'], 2, '.', ',').$space.$space.'
								<input type="hidden" name="total'.$cnt.'" id="total'.$cnt.'" value="'.$row['Total'].'">
							</td>'
						."</tr>";
					$cnt++;
					$price += round(($row['Total']/(100+$row['Vat'])*100),2);
					$vat += round(($row['Total']/(100+$row['Vat'])*7),2);
					$total += $row['Total'];
				}
			}
			$tmp = $tmp.$sqlCus."<br>";
		}
	}
	echo 		'<tr>
					<td colspan="5" style="text-align:center;"><b>รวม<b></td>
					<td style="text-align:right;"><span id="price">'.number_format($price, 2, '.', ',').'</span></td>
					<td style="text-align:right;"><span id="vat">'.number_format($vat, 2, '.', ',').'</span></td>
					<td style="text-align:right;">
						<span id="total">'.number_format($total, 2, '.', ',').'</span>
						<input type="hidden" name="TotalPrice" id="total_text" value="'.$total.'">
					</td>
				</tr>';
	echo	'</table>
			<input type="hidden" name="Cnt" id="Cnt" value="'.($cnt-1).'">
			<input type="hidden" name="checkCnt" id="checkCnt" value="'.$_SESSION['BillCnt'].'">';
?>
