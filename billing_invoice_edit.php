<html>
<head>
	<?php
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>การแก้ไขใบวางบิลเครดิต</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">การแก้ไขใบวางบิลเครดิต</p></h2>
			<div class="margin-lr box center">
				<form action="billing_invoice_edit_process.php?id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								$strSQL = "SELECT * FROM BillingInvoice WHERE ID='".$_GET['id']."'";
								$result = mysqli_query($conn,$strSQL);
								$row = mysqli_fetch_assoc($result);
								
								$TH_Month = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
								$dayStart = date("d",strtotime($row['DateStart']));
								$monthStart = date("m",strtotime($row['DateStart']));
								$yearStart = date("Y",strtotime($row['DateStart']));
								$dayEnd = date("d",strtotime($row['DateEnd']));
								$monthEnd = date("m",strtotime($row['DateEnd']));
								$yearEnd = date("Y",strtotime($row['DateEnd']));
								$customer = $row['CustomerID'];
								// ID
								$id = ($_SESSION['ID']+1);
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td>
													เลขที่
												</td>
												<td>
													'.$row['Code'].'
												</td>
											</tr>
											<tr>
												<td style="width:90px">วันที่</td>
												<td colspan="2">
													<select id="dayStart" name="dayStart" onchange="SelectBillingInvoiceByCustomer(this.value,\'dayStart\')">';
								for ($i=1; $i <= 31 ; $i++) { 
									echo '<option value="'.$i.'" ';
										if (isset($dayStart) AND $i == $dayStart) {
											echo 'selected';
										}
									echo'>'.$i.'</option>';
								}
								echo				'</select>
													<select id="monthStart" name="monthStart" onchange="SelectBillingInvoiceByCustomer(this.value,\'monthStart\')">';
								for ($i=1; $i <= 12 ; $i++) { 
									echo '<option value="'.$i.'"';
									if (isset($monthStart) AND $i == $monthStart) {
										echo 'selected';
									}
									echo '>'.$TH_Month[$i-1].'</option>';
								}
								echo				'
													</select>
													<select id="yearStart" name="yearStart" onchange="SelectBillingInvoiceByCustomer(this.value,\'yearStart\')">';
								for ($i=0; $i < 5 ; $i++) { 
									echo '<option value="'.(date("Y",strtotime("-1 month -".$i." year"))).'"';
									if (isset($yearStart) AND (date("Y",strtotime("-1 month"))-$i+543) == $yearStart) {
										echo 'selected';
									}
									echo '>'.(date("Y",strtotime("-1 month"))-$i+543).'</option>';
								}
								echo				'
													</select>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="2">
													<select id="dayEnd" name="dayEnd" onchange="SelectBillingInvoiceByCustomer(this.value,\'dayEnd\')">';
								for ($i=1; $i <= 31 ; $i++) { 
									echo '<option value="'.$i.'" ';
									if (isset($dayEnd) AND $i == $dayEnd) {
										echo 'selected';
									}
									echo'>'.$i.'</option>';
								}
								echo				'</select>
													<select id="monthEnd" name="monthEnd" onchange="SelectBillingInvoiceByCustomer(this.value,\'monthEnd\')">';
								for ($i=1; $i <= 12 ; $i++) { 
									echo '<option value="'.$i.'"';
									if (isset($monthEnd) AND $i == $monthEnd) {
										echo 'selected';
										$isSelected = true;
									}
									echo '>'.$TH_Month[$i-1].'</option>';
								}
								echo				'
													</select>
													<select id="yearEnd" name="yearEnd" onchange="SelectBillingInvoiceByCustomer(this.value,\'yearEnd\')">';
								for ($i=0; $i < 5 ; $i++) { 
									echo '<option value="'.(date("Y")-$i).'"';
									if (isset($yearEnd) AND date("Y")-$i+543 == $yearEnd) {
										echo 'selected';
									}
									echo '>'.(date("Y")-$i+543).'</option>';
								}
								echo				'
													</select>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px">ค้นหาชื่อ</td>
												<td>
													<input class="name" name="InvoiceCustomerName" type="text" onkeyup="SearchWord(this.value);"/> 
													<a class="btn margin-lr smooth" href="customer_add.php?page=billing_invoice_edit">เพิ่มลูกค้า</a>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="NameList">';
								$_SESSION['CustomerID'] = $row['CustomerID'];
								include('namelist.php');
								echo				'</span>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="CustomerDetail">';
								$_SESSION['customer'] = $row['CustomerID'];
								include('customer_detail.php');
								echo				'</span>
												</td>
											</tr>
										</table>
										<div class="margin-b" id="BillingInvoiceList">';
								$_SESSION['dayStart'] = $dayStart;
								$_SESSION['monthStart'] = $monthStart;
								$_SESSION['yearStart'] = $yearStart;
								$_SESSION['dayEnd'] = $dayEnd;
								$_SESSION['monthEnd'] = $monthEnd;
								$_SESSION['yearEnd'] = $yearEnd;
								$_SESSION['customer'] = $customer;
								$_SESSION['InvoiceList'] = $row['InvoiceList'];
								$_SESSION['BillCnt'] = $row['BillCnt'];
								include('billing_invoice_select_by_customer.php');
								echo	'</div>';
								// echo "dayStart:".$_SESSION['dayStart']."<br>";
								// echo "monthStart:".$_SESSION['monthStart']."<br>";
								// echo "yearStart:".$_SESSION['yearStart']."<br>";
								// echo "dayEnd:".$_SESSION['dayEnd']."<br>";
								// echo "monthEnd:".$_SESSION['monthEnd']."<br>";
								// echo "yearEnd:".$_SESSION['yearEnd']."<br>";
								// echo "customer:".$_SESSION['customer']."<br>";
							?>
						</div>
					</div>
					<input class="btn smooth" type="submit" value="แก้ไขบิล"></a>
					<a class="btn smooth" href="billing_invoice.php">กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>