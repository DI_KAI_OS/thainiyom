<html>
<head>
	<?php
		include("connection.php");
		$space = "&nbsp;&nbsp;&nbsp;&nbsp;";
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>ดูใบวางบิลใบส่งของ</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ดูใบวางบิลใบส่งของ</p></h2>
			<div class="margin-lr box center">
				<div class="text-left margin-b">
					<div class="col">
						<?php
							$sql = "SELECT * FROM BillingInvoice WHERE ID='".$_GET['id']."'";
							$result = mysqli_query($conn,$sql);
							$row = mysqli_fetch_assoc($result);
							$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
							$resultCus = mysqli_query($conn,$sqlCus);
							$rowCus = mysqli_fetch_assoc($resultCus);

							// print_r($row);
							// echo "<br>";
							$billList = explode("-", $row['InvoiceList']);
							// print_r($billList);
							// echo "<br>";

							echo 'เล่มที่ '.(date("y",strtotime($row['DateEnd']))+43)."<br>"
								.'เลขที่ '.$row['Code']."<br>"
								.'วันที่ '.date("d/m/").(date("Y")+543)."<br>"
								.$rowCus['Name']."<br>"
								.'รวม '.$row['BillCnt']." ฉบับ<br>";
							echo '
								<table>
									<tr>
										<th width="80px">ลำดับที่</th>
										<th width="80px">เลขที่บิล</th>
										<th width="80px">วันที่</th>
										<th>จำนวนเงิน</th>
									</tr>';
							for ($i=1; $i <= $row['BillCnt'] ; $i++) { 
								$sqlCre = "SELECT * FROM Invoice WHERE ID='".$billList[($i-1)]."'";
								// echo $sqlCre." ".$i."<br>";
								$resultCre = mysqli_query($conn,$sqlCre);
								$rowCre = mysqli_fetch_assoc($resultCre);
								$yearID = (date("y",strtotime($rowCre['Date'])))+43;
								if ($yearID > 100) {
									$yearID -= 100;
								}
								$id = "TNY".$yearID."-";
								for ($j=0; $j < 4-strlen($rowCre['Code']); $j++) { 
									$id .= "0";
								}
								$id .= $rowCre['Code'];
								echo '<tr>
										<td style="text-align:center;">'.$i.'</td>
										<td>'.$id.'</td>
										<td>'.date("d/m/",strtotime($rowCre['Date'])).(date("Y",strtotime($rowCre['Date']))+543).'</td>'
										."<td ";
								if ($i%2 == 1) {
									echo "style='background-color:#B4F082;";
								}else{
									echo "style='background-color:#99E857;";
								}
								echo "text-align:right;'>".number_format($rowCre['Total'], 2, '.', ',').$space.$space."</td>"
									.'</tr>';
							}
							echo	'</table>';
							echo 'รวมเงิน '.number_format($row['Total'], 2, '.', ',');
						?>
					</div>
				</div>
				<a href='print.php?type=billing_invoice&id=<?php echo $_GET['id']; ?>' class='btn smooth' target="_blank">ดูตัวอย่างก่อนพิมพ์</a>
				<a href='billing_invoice_edit.php?id=<?php echo $_GET['id']; ?>' class='btn smooth'>แก้ไข</a>
				<a href='billing_invoice.php' class='btn smooth'>กลับ</a>
			</div>
		</div>
	</div>
</body>
</html>