<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

include("connection.php");

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$head_bold_style = array(
 'alignment' => array(
   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
 ),
 'font'  => array(
    'bold'  => true,
    'size'  => 16,
    'name'  => 'AngsanaUPC'
  )
);
$head_style = array(
 'alignment' => array(
   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
 ),
 'font'  => array(
        'size'  => 16,
        'name'  => 'AngsanaUPC'
    )
);
$left_style = array(
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
  ),
);
$default_content_style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
	'font'  => array(
		'size'  => 14,
		'name'  => 'AngsanaUPC'
	)
);
$main_branch_style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
	'font'  => array(
		'bold'  => true,
		'size'  => 14,
		'name'  => 'AngsanaUPC'
	)
);
$default_name_content_style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'font'  => array(
		'size'  => 14,
		'name'  => 'AngsanaUPC'
	)
);
$default_number_style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
	'font'  => array(
		'size'  => 14,
		'name'  => 'AngsanaUPC'
	)
);
$total_style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'font'  => array(
		'size'  => 14,
		'name'  => 'AngsanaUPC'
	)
);
$BStyle = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$BAllStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$BDoubleStyle = array(
  'borders' => array(
    'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_DOUBLE
    )
  )
);

// Set Page Layout
$objPHPExcel->setActiveSheetIndex(0)
				    ->getPageSetup()
				    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->setActiveSheetIndex(0)
				    ->getPageSetup()
				    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$objPHPExcel->setActiveSheetIndex(0)
    				->getPageMargins()->setTop(0);
$objPHPExcel->setActiveSheetIndex(0)
    				->getPageMargins()->setRight(0);
$objPHPExcel->setActiveSheetIndex(0)
    				->getPageMargins()->setLeft(0);
$objPHPExcel->setActiveSheetIndex(0)
    				->getPageMargins()->setBottom(0);

// Set Date in thai
$date = explode("-", $_GET['date']);
$thai_month_arr=array(
		"0"=>"",
		"1"=>"มกราคม",
		"2"=>"กุมภาพันธ์",
		"3"=>"มีนาคม",
		"4"=>"เมษายน",
		"5"=>"พฤษภาคม",
		"6"=>"มิถุนายน",
		"7"=>"กรกฎาคม",
		"8"=>"สิงหาคม",
		"9"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม"
);

// Set width
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('A')
						->setWidth(4.25);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('B')
						->setWidth(8.63);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('C')
						->setWidth(8.63);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('D')
						->setWidth(56.75);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('E')
						->setWidth(14.63);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('F')
						->setWidth(8);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('G')
						->setWidth(8);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('H')
						->setWidth(11.50);
$objPHPExcel->setActiveSheetIndex(0)
						->getColumnDimension('I')
						->setWidth(11.50);


function addFormHeader($page){
	$startRow = ($page-1)*31;

	// Set variable
	$objPHPExcel = $GLOBALS['objPHPExcel'];
	$head_bold_style = $GLOBALS['head_bold_style'];
	$head_style = $GLOBALS['head_style'];
	$left_style = $GLOBALS['left_style'];
	$default_content_style = $GLOBALS['default_content_style'];
	$default_name_content_style = $GLOBALS['default_name_content_style'];
	$total_style = $GLOBALS['total_style'];
	$BStyle = $GLOBALS['BStyle'];
	$BAllStyle = $GLOBALS['BAllStyle'];
	$BDoubleStyle = $GLOBALS['BDoubleStyle'];
	$date = $GLOBALS['date'];
	$thai_month_arr = $GLOBALS['thai_month_arr'];
	$main_branch_style = $GLOBALS['main_branch_style'];
	$default_number_style = $GLOBALS['default_number_style'];

	// Set Height
	$objPHPExcel->setActiveSheetIndex(0)
	            ->getRowDimension($startRow+1)
	            ->setRowHeight(21);
	for ($i=1; $i <= 5; $i++) {
		$objPHPExcel->setActiveSheetIndex(0)
		->getRowDimension($startRow+$i)
		->setRowHeight(21);
	}
	for ($i=6; $i <= 9; $i++) {
		$objPHPExcel->setActiveSheetIndex(0)
		->getRowDimension($startRow+$i)
		->setRowHeight(18.75);
	}
	for ($i=10; $i <= 29; $i++) {
		$objPHPExcel->setActiveSheetIndex(0)
		->getRowDimension($startRow+$i)
		->setRowHeight(18);
	}
	$objPHPExcel->setActiveSheetIndex(0)
							->getRowDimension($startRow+30)
							->setRowHeight(21.75);
	$objPHPExcel->setActiveSheetIndex(0)
							->getRowDimension($startRow+31)
							->setRowHeight(18.75);

	// Set Cell Style
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("A".($startRow+1).":I".($startRow+2))
							->applyFromArray($head_bold_style);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("A".($startRow+3).":I".($startRow+3))
							->applyFromArray($head_style);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("A".($startRow+4).":I".($startRow+5))
							->applyFromArray($left_style);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("E".($startRow+5))
							->getNumberFormat()
							->setFormatCode('0');
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("A".($startRow+6).":I".($startRow+9))
							->applyFromArray($default_content_style);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle("H".($startRow+6).":I".($startRow+7))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle("H".($startRow+8).":I".($startRow+9))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".($startRow+6).":G".($startRow+9))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("B".($startRow+6).":C".($startRow+7))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("A".($startRow+8).":A".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("B".($startRow+8).":B".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("C".($startRow+8).":C".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("D".($startRow+6).":D".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("E".($startRow+6).":E".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("E".($startRow+10).":E".($startRow+29))
							->getNumberFormat()
							->setFormatCode('0');
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("F".($startRow+6).":G".($startRow+7))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("F".($startRow+8).":F".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("G".($startRow+8).":G".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("H".($startRow+6).":H".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("I".($startRow+6).":I".($startRow+9))
							->applyFromArray($BStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("A".($startRow+10).":I".($startRow+29))
							->applyFromArray($default_content_style)
							->applyFromArray($BAllStyle);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("F".($startRow+10).":F".($startRow+29))
							->applyFromArray($main_branch_style);
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("H".($startRow+10).":H".($startRow+30))
							->applyFromArray($default_number_style)
							->getNumberFormat()
							->setFormatCode('#,##0.00');
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("I".($startRow+10).":I".($startRow+30))
							->applyFromArray($default_number_style)
							->getNumberFormat()
							->setFormatCode('#,##0.00');
	$objPHPExcel->setActiveSheetIndex(0)
							->getStyle("H".($startRow+30).":I".($startRow+30))
							->applyFromArray($BDoubleStyle);

	// Add Header
	$objPHPExcel->setActiveSheetIndex(0)
	            ->mergeCells("A".($startRow+1).":I".($startRow+1))
	            ->setCellValue("A".($startRow+1), 'รายงานภาษีขาย')
	            ->mergeCells("A".($startRow+2).":I".($startRow+2))
	            ->setCellValue("A".($startRow+2), "ประจำเดือน ".$thai_month_arr[intval($date[1])]." ".($date[0]+543))
	            ->mergeCells("A".($startRow+3).":I".($startRow+3))
	            ->setCellValue("A".($startRow+3), "ชื่อผู้ประกอบการ  นายกมล  อนันตประยูร  ที่อยู่  856/11-12  ถ.เจตน์จำนงค์  ต.บางปลาสร้อย  อ.เมืองชลบุรี  จ.ชลบุรี  20000")
	            ->setCellValue("B".($startRow+4), "ชื่อสถานประกอบการ")
	            ->setCellValue("E".($startRow+4), "ไทยนิยมฮาร์ดแวร์")
	            ->setCellValue("B".($startRow+5), "เลขประจำตัวผู้เสียภาษีอากร")
	            ->setCellValue("E".($startRow+5), "3209900478617")
							->mergeCells("B".($startRow+6).":C".($startRow+7))
							->setCellValue("B".($startRow+6), "ใบกำกับภาษี")
							->mergeCells("A".($startRow+8).":A".($startRow+9))
							->setCellValue("A".($startRow+8), "ลำดับ")
							->mergeCells("B".($startRow+8).":B".($startRow+9))
							->setCellValue("B".($startRow+8), "วัน/เดือน/ปี")
							->mergeCells("C".($startRow+8).":C".($startRow+9))
							->setCellValue("C".($startRow+8), "เล่มที่/เล่มที่")
							->mergeCells("D".($startRow+6).":D".($startRow+9))
							->setCellValue("D".($startRow+6), "ผู้ซื้อสินค้า/ผู้รับบริการ")
							->setCellValue("E".($startRow+6), "เลขประจำตัวผู้เสีย")
							->setCellValue("E".($startRow+7), "ภาษีอากร")
							->setCellValue("E".($startRow+8), "ของผู้ซื้อสินค้า/")
							->setCellValue("E".($startRow+9), "ผู้รับบริการ")
							->mergeCells("F".($startRow+6).":G".($startRow+7))
							->setCellValue("F".($startRow+8), "สำนักงาน")
							->setCellValue("F".($startRow+9), "ใหญ่")
							->mergeCells("G".($startRow+8).":G".($startRow+9))
							->setCellValue("G".($startRow+8), "สาขาที่ ..")
							->mergeCells("H".($startRow+6).":H".($startRow+7))
							->setCellValue("H".($startRow+6), "มูลค่าของสินค้า")
							->mergeCells("H".($startRow+8).":H".($startRow+9))
							->setCellValue("H".($startRow+8), "หรือบริการ")
							->mergeCells("I".($startRow+6).":I".($startRow+7))
							->setCellValue("I".($startRow+6), "จำนวนเงิน")
							->mergeCells("I".($startRow+8).":I".($startRow+9))
							->setCellValue("I".($startRow+8), "ภาษีมูลค่าเพิ่ม")
							->setCellValue("D".($startRow+30), "ยอดรวม");
}

$sql = "SELECT * FROM (
				(SELECT VatCredit.ID, VatCredit.Date, VatCredit.CustomerID, VatCredit.Vat, VatCredit.Total, VatCredit.Code, '2' AS ord, 'bill_credit' AS title FROM VatCredit)
				UNION ALL
				(SELECT VatCash.ID, VatCash.Date, VatCash.CustomerID, VatCash.Vat, VatCash.Total, VatCash.Code, '1' AS ord, 'bill_cash' AS title FROM VatCash)
				UNION ALL
				(SELECT TaxInvoice.ID, TaxInvoice.Date, '-1' AS CustomerID, TaxInvoice.Vat, TaxInvoice.Total, TaxInvoice.Code, '0' AS ord, 'tax_invoice' AS title FROM TaxInvoice)
		) results
		WHERE Date LIKE '%".$_GET['date']."%'
		ORDER BY Date ASC, ord ASC, Code ASC";

if($result = mysqli_query($conn,$sql)){
	$cnt = 1;
	$tmp = "";
	$arrayDate = array();
	$totalnovat = 0;
	$vat = 0;
	$total = 0;
	$first = 1;
	$allPrice = 0;
	$allVat = 0;
	$allTotal = 0;
	while($row = mysqli_fetch_assoc($result)){
		// new page
		if ($cnt%20 == 1) {
			addFormHeader(floor(($cnt-1)/20)+1);

			$startRowTotal = 30+(floor(($cnt-1)/20)*31);
			$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue("H".($startRowTotal), "=SUM(H".($startRowTotal-20).":H".($startRowTotal-1).")")
									->setCellValue("I".($startRowTotal), "=SUM(I".($startRowTotal-20).":I".($startRowTotal-1).")");
		}

		$yearID = (date("y",strtotime($row['Date'])))+43;
		if ($yearID > 100) {
			$yearID -= 100;
		}
		$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
		$resCus = mysqli_query($conn, $sqlCus);
		$rowCus = mysqli_fetch_assoc($resCus);
		// Test
		// echo "</br>".$row['title'];
		$subtitle = "";
		switch ($row['title']) {
			case 'bill_credit':
				$subtitle = "credit";
				// Calcucate Vat & Total
				$totalnovat = round(($row['Total']/(100+$row['Vat'])*100),2);
				$vat = round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2);
				$total = $row['Total'];
				// ID
				$id = "IV".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) {
					$id .= "0";
				}
				$id .= $row['Code'];
				break;
			case 'bill_cash':
				$subtitle = "cash";
				// Calcucate Vat & Total
				$totalnovat = round(($row['Total']/(100+$row['Vat'])*100),2);
				$vat = round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2);
				$total = $row['Total'];
				// ID
				$id = "CR".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) {
					$id .= "0";
				}
				$id .= $row['Code'];
				break;
			case 'tax_invoice':
				$subtitle = "taxinvoice";
				$rowCus['Name'] = "เงินสด";
				$rowCus['IsMain'] = "สำนักงานใหญ่";
				$rowCus['TaxID'] = "-";
				// Calcucate Vat & Total
				$percentVat = 7;
				$totalnovat = round(($row['Total']/(100+$percentVat)*100),2);
				$vat = round(($row['Total']/(100+$percentVat)*$percentVat),2);
				$total = $row['Total'];
				// ID
				$id = "CH".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) {
					$id .= "0";
				}
				$id .= $row['Code'];
				break;

			default:
				# code...
				break;
		}
		$href = "'".$row['title']."_view.php?".$subtitle."id=".$row['ID']."&back=sales_tax_view&date=".date("Y-m",strtotime($row['Date']))."'";
		$year = date("Y",strtotime($row['Date']))+543;

		// echo '<tr class="view" onclick="document.location = '.$href.';">
		// 	<td style="text-align:center;">'.$id.'</td>
		// 	<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
		// 	<td>'.$rowCus['Name'].'</td>
		// 	<td style="text-align:right;">'.number_format($totalnovat, 2, '.', ',').$space.$space.'</td>
		// 	<td style="text-align:right;">'.number_format($vat, 2, '.', ',').$space.$space.'</td>'
		// ."<td ";
		// if ($cnt%2 == 1) {
		// 	echo "style='background-color:#B4F082;";
		// }else{
		// 	echo "style='background-color:#99E857;";
		// }
		// echo "text-align:right;'>".number_format($total, 2, '.', ',').$space.$space."</td>"
		// 	."</tr>";

		// Add Content <sale tax>
		$startRowContent = 9+(floor(($cnt-1)/20)*11);
		$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue("A".($startRowContent+$cnt), $cnt)
								->setCellValue("B".($startRowContent+$cnt), date("d-m-",strtotime($row['Date'])).$year)
								->setCellValue("C".($startRowContent+$cnt), $id)
								->setCellValue("D".($startRowContent+$cnt), $rowCus['Name'])
								->setCellValue("E".($startRowContent+$cnt), $rowCus['TaxID'])
								->setCellValue("H".($startRowContent+$cnt), $totalnovat)
								->setCellValue("I".($startRowContent+$cnt), $vat);
		if ($rowCus['IsMain'] == "สำนักงานใหญ่") {
			$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue("F".($startRowContent+$cnt), "O");
		}elseif ($rowCus['IsMain'] == "สาขาที่") {
			$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue("G".($startRowContent+$cnt), $rowCus['Branch']);
		}


		$cnt++;
		$_SESSION['ID'] = $row['ID'];
		$allPrice += $totalnovat;
		$allVat += $vat;
		$allTotal += $total;
	}
	$cnt++;
	$_SESSION['ID'] = $row['ID'];
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle("สรุปภาษีขาย ".$thai_month_arr[intval($date[1])]." ".($date[0]+543));
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="สรุปภาษีขาย '.$thai_month_arr[intval($date[1])].' '.($date[0]+543).'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
