<html>
<head>
	<?php
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>การเพิ่มลูกค้า</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">การเพิ่มลูกค้า</p></h2>
			<div class="margin-lr box center">
				<?php
					$href = "customer_add_process.php";
					if (isset($_GET['page'])) {
						$href = $href."?page=".$_GET['page'];
					}
				?>
				<form action="<?php echo $href; ?>" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td style="width:180px">ชื่อ</td>
												<td>
													<input class="name" name="CustomerName" type="text" />
												</td>
											</tr>
											<tr>
												<td>ที่อยู่</td>
												<td>
													<textarea class="name" name="CustomerAddress"></textarea>
												</td>
											</tr>
											<tr>
												<td>เครติด</td>
												<td>
													<input name="CustomerCredit" type="text" value="30" />
												</td>
											</tr>
											<tr>
												<td>เบอร์</td>
												<td>
													<input name="CustomerTel" type="text" />
												</td>
											</tr>
											<tr>
												<td>แฟกซ์</td>
												<td>
													<input name="CustomerFax" type="text" />
												</td>
											</tr>
											<tr>
												<td>สำนักงาน/สาขา</td>
												<td>
													<select name="CustomerIsMain" onchange="branch(this.value)">
														<option value="สำนักงานใหญ่">สำนักงานใหญ่</option>
														<option value="สาขาที่">สาขา</option>
													</select>
													<input style="width:30px" name="CustomerBranch" id="Branch" type="text" />
												</td>
											</tr>
											<tr>
												<td>เลขประจำตัวผู้เสียภาษี</td>
												<td>
													<input name="CustomerTaxID" type="text" />
												</td>
											</tr>
										</table>';
							?>
						</div>
					</div>

					<?php $_SESSION['customer_add'] = strtotime(date('Y-m-d H:i:s')); ?>
					<input type="hidden" name="save_token" value="<?=$_SESSION['customer_add']?>">
					<input class="btn smooth" type="submit" value="เพิ่มลูกค้า"></a>
					<a class="btn smooth" href="customer.php">กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>
