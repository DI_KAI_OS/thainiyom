<html>
<head>
	<?php
		session_start();
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>แก้ไขรายละเอียดลูกค้า</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">แก้ไขรายละเอียดลูกค้า</p></h2>
			<div class="margin-lr box center">
				<form action="customer_edit_process.php?customerid=<?php echo $_GET['customerid']; ?>" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								// Customer
								$sql = "SELECT * FROM Customer WHERE ID='".$_GET['customerid']."'";
								$result = mysqli_query($conn, $sql);
								$rowCus = mysqli_fetch_assoc($result);
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td style="width:180px">ชื่อ</td>
												<td>
													<input class="name" name="CustomerName" type="text" value="'.$rowCus['Name'].'" />
												</td>
											</tr>
											<tr>
												<td>ที่อยู่</td>
												<td><textarea class="name" name="CustomerAddress">'.$rowCus['Address'].'</textarea></td>
											</tr>
											<tr>
												<td>เครติด</td>
												<td>
													<input name="CustomerCredit" type="text" value="'.$rowCus['Credit'].'" />
												</td>
											</tr>
											<tr>
												<td>เบอร์</td>
												<td><input name="CustomerTel" type="text" value="'.$rowCus['Tel'].'" /></td>
											</tr>
											<tr>
												<td>Fax</td>
												<td><input name="CustomerFax" type="text" value="'.$rowCus['Fax'].'" /></td>
											</tr>
											<tr>
												<td>สำนักงาน</td>
												<td>
													<select name="CustomerIsMain" onchange="branch(this.value)">
														<option value="สำนักงานใหญ่" ';
								if ($rowCus['IsMain'] == 'สำนักงานใหญ่') {
									echo 'selected';
								}
								echo 					'>สำนักงานใหญ่</option>
														<option value="สาขาที่" ';
								if ($rowCus['IsMain'] == 'สาขาที่') {
									echo 'selected';
								}
								echo					'>สาขา</option>
													</select>
													<input style="width:30px" name="CustomerBranch" id="Branch" type="text" value="'.$rowCus['Branch'].'" />
												</td>
											</tr>
											<tr>
												<td style="width:130px">เลขประจำตัวผู้เสียภาษี</td>
												<td><input name="CustomerTaxID" type="text" value="'.$rowCus['TaxID'].'" /></td>
											</tr>
										</table>';
								if ($rowCus['IsMain'] == 'สาขาที่') {
									echo '
									<script>
										$(document).ready(function(){
										    $("#Branch").show();
										});
									</script>
									';
								}
							?>
						</div>
					</div>
					
					<?php $_SESSION['cutomer_edit'] = strtotime(date('Y-m-d H:i:s')); ?>
					<input type="hidden" name="save_token" value="<?=$_SESSION['cutomer_edit']?>">
					<input class="btn smooth" type="submit" value="บันทึก"></a>
					<a class="btn smooth" href="customer.php">กลับ</a>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
