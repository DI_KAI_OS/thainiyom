<html>
<head>
	<?php
		include("connection.php");
		$space = "&nbsp;&nbsp;&nbsp;&nbsp;";
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>ดูรายละเอียดลูกค้า</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ดูรายละเอียดลูกค้า</p></h2>
			<div class="margin-lr box center">
				<form action="bill_cash_add_process.php" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								// Customer
								$sql = "SELECT * FROM Customer WHERE ID='".$_GET['customerid']."'";
								$result = mysqli_query($conn, $sql);
								$rowCus = mysqli_fetch_assoc($result);
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td style="width:180px">ชื่อ</td>
												<td>'.$rowCus['Name'].'</td>
											</tr>
											<tr>
												<td>ที่อยู่</td>
												<td>'.$rowCus['Address'].'</td>
											</tr>
												<td>เครติด</td>
												<td>'.$rowCus['Credit'].'</td>
											<tr>
												<td>เบอร์</td>
												<td>'.$rowCus['Tel'].'</td>
											</tr>
											<tr>
												<td>Fax</td>
												<td>'.$rowCus['Fax'].'</td>
											</tr>
											<tr>
												<td>สำนักงาน</td>
												<td>'.$rowCus['IsMain']." ";
								if ($rowCus['IsMain'] == 'สาขาที่') {
									echo ' ';
									$len = 5-strlen($rowCus['Branch']);
									for ($i=0; $i < $len; $i++) { 
										echo '0';
									}
									echo $rowCus['Branch'];
								}
								echo			'</td>
											</tr>
											<tr>
												<td>เลขประจำตัวผู้เสียภาษี</td>
												<td>'.$rowCus['TaxID'].'</td>
											</tr>
										</table>';
							?>
						</div>
					</div>
					<a class="btn smooth" href="customer_edit.php?customerid=<?php echo $_GET['customerid']; ?>">แก้ไข</a>
					<?php
		                $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
		                echo "<a href='$url' class='btn'>กลับ</a>"; 
		            ?>
				</form>
			</div>
		</div>
	</div>
</body>
</html>