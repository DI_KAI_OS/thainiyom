<html>
<head>
	<?php
		session_start();
		include("connection.php");
    ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
	<title>ใบส่งของ</title>
</head>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ใบส่งของ</p></h2>
			<div class="margin-lr box center">
				<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
				<span id="Pagination">
					<?php
						if ($_SESSION['orderlist'] != "invoice") {
							$_SESSION['orderlist'] = "invoice";
	            			$_SESSION['orderlistpage'] = 1;
						}
						include("pagination.php");
					?>
				</span>
	            <div class="table-responsive" id="OrderList">
	            	<?php
	            		unset($_SESSION['cusName']);
	            		include("orderlist.php");
	            	?>
	            </div>
	        </div>
		</div>
	</div>
</body>
</html>