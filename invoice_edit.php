<html>
<head>
	<?php
		session_start();
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>แก้ไขใบส่งของ</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">แก้ไขใบส่งของ</p></h2>
			<div class="margin-lr box center">
	            <?php
					if (isset($_GET['back'])) {
						if (isset($_GET['date'])) {
							$href1 = "'invoice_edit_process.php?invoiceid=".$_GET['invoiceid']."&back=".$_GET['back']."&date=".$_GET['date']."'";
							$href2 = "'".$_GET['back'].".php?date=".$_GET['date']."'";
						}else{
							$href1 = "'invoice_edit_process.php?invoiceid=".$_GET['invoiceid']."&back=".$_GET['back']."'";
							$href2 = "'".$_GET['back'].".php'";
						}
					}else{
						$href1 = "'invoice_edit_process.php?invoiceid=".$_GET['invoiceid']."'";
						$href2 = "'invoice.php'";
					}
	            ?>
				<form action=<?php echo $href1; ?> method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								// Invoice
								$sql = "SELECT * FROM Invoice WHERE ID='".$_GET['invoiceid']."'";
								$result = mysqli_query($conn, $sql);
								$row = mysqli_fetch_assoc($result);
								// Customer
								$sql = "SELECT * FROM Customer";
								if ($row['CustomerID'] != 0) {
									$sql = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
									$result = mysqli_query($conn, $sql);
									$rowCus = mysqli_fetch_assoc($result);
									$_SESSION['CustomerID'] = $row['CustomerID'];
								}
								// Change to Buddha Year
								$year = date("Y",strtotime($row['Date']))+543;
								// ID
								$yearID = (date("y",strtotime($row['Date'])))+43;
								if ($yearID > 100) {
									$yearID -= 100;
								}
								$id = "TNY".$yearID."-";
								for ($i=0; $i < 4-strlen($row['Code']); $i++) {
									$id .= "0";
								}
								$id .= $row['Code'];
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td style="width:90px">เลขที่บิล</td>
												<td colspan="2">'.$id.'</td>
											</tr>
												<td style="width:90px">วันที่</td>
												<td colspan="2"><input name="InvoiceDate" type="text" value="'.date("d-m-",strtotime($row['Date'])).$year.'"/></td>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px">ค้นหาชื่อ</td>
												<td>
													<input class="name" name="CashCustomerName" type="text" value="'.$rowCus['Name'].'" onkeyup="SearchWord(this.value)"/>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="NameList">';
								include('namelist.php');
								echo				'</span>
												</td>
											</tr>
											<tr>
												<td></td>
												<td colspan="4">
													<span id="CustomerDetail">
														<table>
															<tr>
																<td style="width:130px">ที่อยู่</td>
																<td>'.$rowCus['Address'].'</td>
															</tr>
															<tr>
																<td style="width:130px">เบอร์</td>
																<td>'.$rowCus['Tel'].'</td>
															</tr>
															<tr>
																<td style="width:130px">Fax</td>
																<td>'.$rowCus['Fax'].'</td>
															</tr>
															<tr>
																<td style="width:130px">สำนักงาน</td>
																<td>'.$rowCus['IsMain'].'</td>
															</tr>
															<tr>
																<td style="width:130px">เลขประจำตัวผู้เสียภาษี</td>
																<td>
																	'.$rowCus['TaxID'].'
																	<input type="hidden" name="LotID" value="'.$row['LotID'].'" />
																</td>
															</tr>
														</table>
													</span>
												</td>
											</tr>
										</table>
										<table class="noborder margin-b list" style="width:auto">
											<tr>
												<td colspan="5"><b style="font-size:1.4em;">รายการสินค้า</b></td>
											</tr>
											<tr>
												<td class="text-cen">เลขที่</td>
												<td class="text-cen">ชื่อ</td>
												<td class="text-cen">จำนวน</td>
												<td class="text-cen">หน่วย</td>
												<td class="text-cen">ราคาต่อหน่วย</td>
												<td style="width: 100px" class="text-cen">รวม</td>
											</tr>';
								$sqlProduct = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
								if ($resultProduct = mysqli_query($conn, $sqlProduct)) {
									$i = 1;
									$total = 0;
									while ($rowProduct = mysqli_fetch_assoc($resultProduct)) {
										$productName = str_replace('"','&quot;',$rowProduct['Name']);
										echo '<tr>
												<td class="text-cen">'.$i.'</td>
												<td>
													<input class="name" name="ProductName'.$i.'" type="text" value="'.$productName.'" />
												</td>
												<td>
													<input name="ProductQuantity'.$i.'" id="ProductQuantity'.$i.'" type="text" value="'.$rowProduct['Quantity'].'" onkeyup="CalPrice(\''.$i.'\')"/>
												</td>
												<td>
													<input name="ProductUnit'.$i.'" type="text" value="'.$rowProduct['Unit'].'"/>
												</td>
												<td>
													<input name="ProductPriceEach'.$i.'" id="ProductPriceEach'.$i.'" type="text" value="'.$rowProduct['PriceEach'].'" onkeyup="CalPrice(\''.$i.'\')"/>
												</td>
												<td class="text-right">
													<span id="ProductTotalText'.$i.'">'.number_format(($rowProduct['Quantity']*$rowProduct['PriceEach']), 2, '.', ',').'</span>
													<input type="hidden" id="ProductTotal'.$i.'" name="ProductTotal'.$i.'" value="'.($rowProduct['Quantity']*$rowProduct['PriceEach']).'" />
												</td>
												<td class="text-cen" class="delete">
													<a href="product_delete_process.php?id='.$rowProduct['ID'].'" class="btn sm smooth" >ลบสินค้า</a>
													<input type="hidden" name="ID'.$i.'" value="'.$rowProduct['ID'].'" />
												</td>
											</tr>';
										$i++;
										$total += $rowProduct['Quantity']*$rowProduct['PriceEach'];
									}
								}
								echo		'
											<tr></tr>
											<tr>
												<td class="text-cen" colspan="5"><b style="font-size:1.5em;">รวมทั้งสิ้น</b></td>
												<td class="text-right">
													<span id="ProductAllTotalText">'.number_format($total, 2, '.', ',').'</span>
													<input type="hidden" id="ProductAllTotal" name="ProductAllTotal" value="'.$total.'" />
												</td>
											</tr>
											<tr></tr>
										</table>
										<table class="noborder margin-b" style="width:auto">
											<tr>
												<td width="220px">
												</td>
												<td>
													<a class="btn smooth del-etc-row float-r">-</a>
													<div class="float-r">&nbsp;</div>
													<a class="btn smooth add-etc-row float-r">+</a>
												</td>
											</tr>
										</table>';
							?>
						</div>
					</div>
					
					<?php $_SESSION['invoice_edit'] = strtotime(date('Y-m-d H:i:s')); ?>
					<input type="hidden" name="save_token" value="<?=$_SESSION['invoice_edit']?>">
					<input class="btn smooth" type="submit" value="บันทึก"></a>
					<a class="btn smooth" href=<?php echo $href2; ?>>กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
					<input type="hidden" id="oldItemSize" name="oldItemSize" value="<?php echo $i-1; ?>" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>
