<?php
	include("connection.php");

	if( $_SESSION['invoice_edit'] == $_POST['save_token'])
	{
		$_SESSION['invoice_edit'] = '';
		if (isset($_GET['back'])) {
			if (isset($_GET['date'])) {
				$href = $_GET['back'].".php?date=".$_GET['date'];
			}else{
				$href = $_GET['back'].".php";
			}
		}else{
			$href = "invoice.php";
		}
		$sql = "SELECT * FROM Invoice WHERE ID='".$_GET['invoiceid']."'";
		if ($result = mysqli_query($conn, $sql)) {
			$rowInv = mysqli_fetch_assoc($result);
			$LotID = $rowInv['LotID'];
		}
	
		$date = explode('-', $_POST['InvoiceDate']);
		$sql = "UPDATE Invoice SET Date='".($date[2]-543)."-".$date[1]."-".$date[0]	//1,2
			."', CustomerID='".$_POST['CustomerID']				//3
			."', Vat='7"										//5
			."', Total='".$_POST['ProductAllTotal']				//7
			."' WHERE ID='".$_GET['invoiceid']."'";
		$text = $sql."<br>";
		for ($i=1; $i <= $_POST['oldItemSize']; $i++) {
			$sqlTmp = "UPDATE Product SET Name='".$_POST['ProductName'.$i]
				."', Quantity='".$_POST['ProductQuantity'.$i]
				."', Unit='".$_POST['ProductUnit'.$i]
				."', PriceEach='".$_POST['ProductPriceEach'.$i]
				."', LotID='".$LotID
				."' WHERE ID='".$_POST['ID'.$i]."'";
			mysqli_query($conn,$sqlTmp);
			$text = $text.$sqlTmp."<br>";
		}
		for ($i=$_POST['oldItemSize']+1; $i < $_POST['oldItemSize'] + $_POST['itemSize']; $i++) {
			$productName = str_replace('"','\"',$_POST['ProductName'.$i]);
			$productName = str_replace("'","\'",$productName);
			$sqlTmp = "INSERT INTO Product VALUE('', '".$productName
				."', '".$_POST['ProductQuantity'.$i]
				."', '".$_POST['ProductUnit'.$i]
				."', '".$_POST['ProductPriceEach'.$i]
				."', '".$LotID
				."')";
			mysqli_query($conn,$sqlTmp);
			$text = $text.$sqlTmp."<br>";
		}
		if(mysqli_query($conn,$sql)){
			$_SESSION['Status'] = "Edit Seccess.";
			header("location:".$href);
		}else{
			$_SESSION['Status'] = "Edit Error.";
			if (isset($_SERVER["HTTP_REFERER"])) {
						header("Location: " . $_SERVER["HTTP_REFERER"]);
				}
		}
		mysqli_close($conn);
	}
?>
