jQuery(function(){
    var counter = parseInt($('#itemSize').val(),10);
    var counterold = parseInt($('#oldItemSize').val(),10);
    var total = 0;
    if($('#oldItemSize').val()){
        total =  counter + counterold - 1;
    }else{
        total = counter;
    }
    $("#Branch").hide();
    jQuery('a.add-etc-row').click(function(event){
        if (total < 15) {
            counter++;
            total++;
            var newRow = jQuery(    '<tr>'+
                                        '<td style="text-align:center;">'+total+'</td>'+
                                        '<td><input class="name" name="ProductName'+total+'" type="text" /></td>'+
                                        '<td><input name="ProductQuantity'+total+'" id="ProductQuantity'+total+'" type="text" value="0" onkeyup="CalPrice(\''+total+'\')"/></td>'+
                                        '<td><input name="ProductUnit'+total+'" type="text" /></td>'+
                                        '<td><input name="ProductPriceEach'+total+'" id="ProductPriceEach'+total+'" type="text" value="0" onkeyup="CalPrice(\''+total+'\')"/></td>'+
                                        '<td class="text-right">'+
                                            '<span id="ProductTotalText'+total+'">0.00</span>'+
                                            '<input type="hidden" id="ProductTotal'+total+'" name="ProductTotal'+total+'" value="0"/>'+
                                        '</td>'+
                                    '</tr>');
            jQuery('table.list tr:last').prev().prev().before(newRow);
            $('#itemSize').val(counter);
        };
    });

    jQuery('a.del-etc-row').click(function(event){
        if (counter > 1) {
            $('.list tr:last').prev().prev().prev().remove();
            total--;
            counter--;
            $('#itemSize').val(counter);
        };
    });

    $(document).on('keypress', ':input:not(textarea):not([type=submit])', function (e) {
        if (e.which == 13) e.preventDefault();
    });
});

function CalPrice(No){
    if (No == 0) {
        //0 = Vat key up
        var tmp = $('#ProductAllTotal').val();
        if (document.getElementById("CreditVat")) {
          var vat = parseFloat($('#CreditVat').val()/100*tmp).toFixed(2);
        }
        if (document.getElementById("CashVat")) {
          var vat = parseFloat($('#CashVat').val()/100*tmp).toFixed(2);
        }
        tmp++; tmp--;
        vat++; vat--;
        $('#Vat').val(vat);
        $('#VatText').text(commaSeparateNumber(parseFloat(vat).toFixed(2)));
        $('#ProductAllTotalAddVat').val(tmp + vat);
        $('#ProductAllTotalAddVatText').text(commaSeparateNumber(parseFloat(tmp + vat).toFixed(2)));
    }else{
        // Price = Quantity * Each Price
        var tmp = parseFloat($('#ProductPriceEach'+No).val() * $('#ProductQuantity'+No).val()).toFixed(2);
        $('#ProductTotalText'+No).text(commaSeparateNumber(tmp));
        $('#ProductTotal'+No).val(tmp);

        // Total
        var totalPrice = 0;
        if ($('#oldItemSize').val() === undefined) {
            var i = $('#itemSize').val();
        }else{
            var i = ($('#oldItemSize').val()*1)+($('#itemSize').val()*1)-1;
        };
        // $('#ProductAllTotal').text(i);
        for (; i > 0; i--) {
            var tmp = $('#ProductTotal'+i).val();
            //change string to number
            totalPrice += parseFloat(tmp, 10)
        };
        $('#ProductAllTotalText').text(commaSeparateNumber(totalPrice.toFixed(2)));
        $('#ProductAllTotal').val(totalPrice.toFixed(2));
        // Vat
        var tmp = $('#ProductAllTotal').val();
        if (document.getElementById("CreditVat")) {
          var vat = parseFloat($('#CreditVat').val()/100*tmp).toFixed(2);
        }
        if (document.getElementById("CashVat")) {
          var vat = parseFloat($('#CashVat').val()/100*tmp).toFixed(2);
        }
        $('#VatText').text(commaSeparateNumber(parseFloat(vat).toFixed(2)));
        $('#Vat').val(vat);
        vat++; vat--;
        $('#ProductAllTotalAddVat').val(totalPrice + vat);
        $('#ProductAllTotalAddVatText').text(commaSeparateNumber(parseFloat(totalPrice + vat).toFixed(2)));
    };
}

function branch(ismain){
    if (ismain == "สำนักงานใหญ่") {
        $("#Branch").hide();
    }else{
        $("#Branch").show();
    };
}

function SearchWord(word){
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("NameList").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("GET","namelist.php?word="+word,true);
    xmlhttp.send();
}

function SelectCustomer(id){
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("CustomerDetail").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("GET","customer_detail.php?id="+id,true);
    xmlhttp.send();

    if (document.getElementById('Deadline')) {
        xmlhttpp = new XMLHttpRequest();
        xmlhttpp.onreadystatechange = function() {
            if (xmlhttpp.readyState == 4 && xmlhttpp.status == 200) {
                document.getElementById("Deadline").innerHTML = xmlhttpp.responseText;
            }
        };
        xmlhttpp.open("GET","Deadline.php?id="+id,true);
        xmlhttpp.send();
    };

    if (document.getElementById("BillingCreditList")) {
        SelectBillingCreditByCustomer(id,'customerid');
    };
    if (document.getElementById("BillingInvoiceList")) {
        SelectBillingInvoiceByCustomer(id,'customerid');
    };
}

function SearchByCustomerName(cusName,page){
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("OrderList").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("GET","orderlist.php?cusName="+cusName+"&page="+page,true);
    xmlhttp.send();
}

function SearchByProductName(proName,page){
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("OrderList").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("GET","orderlist.php?proName="+proName+"&page="+page,true);
    xmlhttp.send();
}

function Pagination(pagination,page){
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("Pagination").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("GET","pagination.php?pagination="+pagination+"&page="+page,true);
    xmlhttp.send();

    xmlhttpp = new XMLHttpRequest();
    xmlhttpp.onreadystatechange = function() {
        if (xmlhttpp.readyState == 4 && xmlhttpp.status == 200) {
            document.getElementById("OrderList").innerHTML = xmlhttpp.responseText;
        }
    };
    xmlhttpp.open("GET","orderlist.php?page="+page,true);
    xmlhttpp.send();
    //orderlist(page);
}

function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
}

function SelectBillingCreditByCustomer(input,type){
    xmlhttp2 = new XMLHttpRequest();
    xmlhttp2.onreadystatechange = function() {
        if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
            document.getElementById("BillingCreditList").innerHTML = xmlhttp2.responseText;
        }
    };
    xmlhttp2.open("GET","billing_credit_select_by_customer.php?input="+input+"&type="+type,true);
    xmlhttp2.send();
}

function BillingCreditCal(){
    var price = 0.0;
    var vat = 0.0;
    var total = 0.0;
    var check = 0;
    for (var i = $('#Cnt').val(); i > 0; i--) {
        if ($('#check'+i).prop('checked')) {
            price = price+parseFloat($('#price'+i).val());
            vat = vat+parseFloat($('#vat'+i).val());
            total = total+parseFloat($('#total'+i).val());
            check++;
        };
    };
    $('#price').text(commaSeparateNumber((parseFloat(price).toFixed(2))));
    $('#vat').text(commaSeparateNumber((parseFloat(vat).toFixed(2))));
    $('#total').text(commaSeparateNumber((parseFloat(total).toFixed(2))));
    $('#total_text').val(total);
    $('#checkCnt').val(check);
}

function SelectBillingInvoiceByCustomer(input,type){
    xmlhttp2 = new XMLHttpRequest();
    xmlhttp2.onreadystatechange = function() {
        if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
            document.getElementById("BillingInvoiceList").innerHTML = xmlhttp2.responseText;
        }
    };
    xmlhttp2.open("GET","billing_invoice_select_by_customer.php?input="+input+"&type="+type,true);
    xmlhttp2.send();
}

function BillingInvoiceCal(){
    var price = 0.0;
    var vat = 0.0;
    var total = 0.0;
    var check = 0;
    for (var i = $('#Cnt').val(); i > 0; i--) {
        if ($('#check'+i).prop('checked')) {
            price = price+parseFloat($('#price'+i).val());
            vat = vat+parseFloat($('#vat'+i).val());
            total = total+parseFloat($('#total'+i).val());
            check++;
        };
    };
    $('#total').text(commaSeparateNumber((parseFloat(total).toFixed(2))));
    $('#total_text').val(total);
    $('#checkCnt').val(check);
}

function CalDeadLine(val, type){
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("Deadline").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("GET","DeadLine.php?type="+type+"&val="+val+"&date="+$("#year").val()+"-"+$("#month").val()+"-"+$("#day").val(),true);
    xmlhttp.send();
}
