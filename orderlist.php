<?php
	session_start();
	include('connection.php');
	$space = "&nbsp;&nbsp;&nbsp;&nbsp;";
	// if not have from GET => use from SESSION
	if (isset($_GET['cusName'])) {
		$_SESSION['cusName'] = $_GET['cusName'];
		$cusName = $_GET['cusName'];
	}elseif (isset($_SESSION['cusName'])) {
		$cusName = $_SESSION['cusName'];
	}

	if (isset($_GET['proName'])) {
		$_SESSION['proName'] = $_GET['proName'];
		$proName = $_GET['proName'];
	}elseif (isset($_SESSION['proName'])) {
		$proName = $_SESSION['proName'];
	}
	// session from first start that page
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	}else{
		$page = $_SESSION['orderlist'];
	}


	unset($_SESSION['ID']);
	unset($_SESSION['dayStart']);
	unset($_SESSION['monthStart']);
	unset($_SESSION['yearStart']);
	unset($_SESSION['dayEnd']);
	unset($_SESSION['monthEnd']);
	unset($_SESSION['yearEnd']);
	unset($_SESSION['customer']);
	// echo $page;
	switch ($page) {
		/////////////////////
		// Vat Credit Page //
		/////////////////////
		case 'credit':
			// echo "cusName = ".$cusName."-".$_SESSION['cusName']."<br>";
			// echo "proName = ".$proName."-".$_SESSION['proName']."<br>";
			echo '
				<table>
						<tr>
							<th width="60px">เลขที่บิล</th>
							<th width="80px">วันที่</th>
							<th>ชื่อลูกค้า</th>
							<th width="100px">เลขที่ใบสั่งซื้อ</th>
							<th>ราคา</th>
							<th>Vat</th>
							<th>ราคารวม</th>
						</tr>';
			// Customer Part
			$cus = array('cnt' => 0, );
			$sqlCus = "SELECT * FROM Customer";
			if ($cusName != "") {
				$sqlCus .= " WHERE Name LIKE '%".$cusName."%'";
			}
			if ($resCus = mysqli_query($conn, $sqlCus)) {
				while($rowCus = mysqli_fetch_assoc($resCus)){
					$rowCus['cnt'] = $cus['cnt'];
					array_push($cus, $rowCus);
					$cus['cnt']++;
				}
			}
			// echo $cus['cnt'];
			// Product Part
			$pro = array('cnt' => 0, );
			$sqlPro = "SELECT * FROM Product";
			if ($proName != "") {
				$sqlPro .= " WHERE Name LIKE '%".$proName."%'";
			}
			if ($resPro = mysqli_query($conn, $sqlPro)) {
				while ($rowPro = mysqli_fetch_assoc($resPro)) {
					$rowPro['cnt'] = $pro['cnt'];
					array_push($pro, $rowPro);
					$pro['cnt']++;
				}
			}
			// echo " ".$pro['cnt'];

			$strSQL = "SELECT * FROM VatCredit";
			$topCode = 0;
			if ($result = mysqli_query($conn,$strSQL)) {
				while($row = mysqli_fetch_assoc($result)){
					if (date("Y",strtotime($row['Date'])) == date("Y")) {
						if ($topCode < $row['Code']) {
							$topCode = $row['Code'];
						}
					}
				}
			}
			if ($cus['cnt'] > 0 AND $pro['cnt'] > 0) {
				$strSQL .= " WHERE";
			}
			// Where Customer in strSQL
			if ($cus['cnt'] > 0) {
				for ($i=0; $i < $cus['cnt']; $i++) { 
					if ($cus[$i]['cnt'] != 0) {
						$strSQL .= " OR CustomerID='".$cus[$i]['ID']."'";
					}else{
						$strSQL .= " (CustomerID='".$cus[$i]['ID']."'";
					}
				}
				$strSQL .= ")";
			}

			//Where Product in strSQL
			if ($pro['cnt'] > 0) {
				if ($cus['cnt'] > 0) {
					$strSQL .= " AND";
				}
				for ($i=0; $i < $pro['cnt']; $i++) { 
					if ($pro[$i]['cnt'] != 0) {
						$strSQL .= " OR LotID='".$pro[$i]['LotID']."'";
					}else{
						$strSQL .= " (LotID='".$pro[$i]['LotID']."'";
					}
				}
				$strSQL .= ")";
			}
			$strSQL .= " ORDER BY Date DESC, ID DESC LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			// echo "<br>".$strSQL;
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// ID
					$yearID = (date("y",strtotime($row['Date'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = "IV".$yearID."-";
					for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
						$id .= "0";
					}
					$id .= $row['Code'];
					// Customer Name
					$sqlCus = "SELECT Name FROM Customer WHERE ID='".$row['CustomerID']."'";
					$resCus = mysqli_query($conn,$sqlCus);
					$rowCus = mysqli_fetch_assoc($resCus);

					$href = "'bill_credit_view.php?creditid=".$row['ID']."'";
					$year = date("Y",strtotime($row['Date']))+543;
					echo '<tr class="view" onclick="document.location = '.$href.';">
							<td style="text-align:center;">'.$id.'</td>
							<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
							<td>'.$rowCus['Name'].'</td>
							<td style="text-align:center;">'.$row['PO'].'</td>
							<td style="text-align:right;">'.number_format(round(($row['Total']/(100+$row['Vat'])*100),2), 2, '.', ',').$space.$space.'</td>
							<td style="text-align:right;">'.number_format(round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2), 2, '.', ',').$space.$space.'</td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>".number_format($row['Total'], 2, '.', ',').$space.$space."</td>"
						."</tr>";
					$cnt++;
					$_SESSION['ID'] = $topCode;
					$tmp = $tmp.$sqlCus."<br>";
				}
			}
			echo	'</table>';
			//echo $tmp;
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		///////////////////
		// Vat Cash Page //
		///////////////////
		case 'cash':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่บิล</th>
						<th width="80px">วันที่</th>
						<th>ชื่อลูกค้า</th>
						<th>ราคา</th>
						<th>Vat</th>
						<th>ราคารวม</th>
					</tr>';
			// Customer Part
			$cus = array('cnt' => 0, );
			$sqlCus = "SELECT * FROM Customer";
			if ($cusName != "") {
				$sqlCus .= " WHERE Name LIKE '%".$cusName."%'";
			}
			if ($resCus = mysqli_query($conn, $sqlCus)) {
				while($rowCus = mysqli_fetch_assoc($resCus)){
					$rowCus['cnt'] = $cus['cnt'];
					array_push($cus, $rowCus);
					$cus['cnt']++;
				}
			}
			// echo $cus['cnt'];
			// Product Part
			$pro = array('cnt' => 0, );
			$sqlPro = "SELECT * FROM Product";
			if ($proName != "") {
				$sqlPro .= " WHERE Name LIKE '%".$proName."%'";
			}
			if ($resPro = mysqli_query($conn, $sqlPro)) {
				while ($rowPro = mysqli_fetch_assoc($resPro)) {
					$rowPro['cnt'] = $pro['cnt'];
					array_push($pro, $rowPro);
					$pro['cnt']++;
				}
			}
			// echo " ".$pro['cnt'];

			$strSQL = "SELECT * FROM VatCash";
			$topCode = 0;
			if ($result = mysqli_query($conn,$strSQL)) {
				while($row = mysqli_fetch_assoc($result)){
					if (date("Y",strtotime($row['Date'])) == date("Y")) {
						if ($topCode < $row['Code']) {
							$topCode = $row['Code'];
						}
					}
				}
			}
			if ($cus['cnt'] > 0 AND $pro['cnt'] > 0) {
				$strSQL .= " WHERE";
			}
			// Where Customer in strSQL
			if ($cus['cnt'] > 0) {
				for ($i=0; $i < $cus['cnt']; $i++) { 
					if ($cus[$i]['cnt'] != 0) {
						$strSQL .= " OR CustomerID='".$cus[$i]['ID']."'";
					}else{
						$strSQL .= " (CustomerID='".$cus[$i]['ID']."'";
					}
				}
				$strSQL .= ")";
			}

			//Where Product in strSQL
			if ($pro['cnt'] > 0) {
				if ($cus['cnt'] > 0) {
					$strSQL .= " AND";
				}
				for ($i=0; $i < $pro['cnt']; $i++) { 
					if ($pro[$i]['cnt'] != 0) {
						$strSQL .= " OR LotID='".$pro[$i]['LotID']."'";
					}else{
						$strSQL .= " (LotID='".$pro[$i]['LotID']."'";
					}
				}
				$strSQL .= ")";
			}
			$strSQL .= " ORDER BY Date DESC, ID DESC LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// ID
					$yearID = (date("y",strtotime($row['Date'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = "CR".$yearID."-";
					for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
						$id .= "0";
					}
					$id .= $row['Code'];
					// Customer Name
					$sqlCus = "SELECT Name FROM Customer WHERE ID='".$row['CustomerID']."'";
					$resCus = mysqli_query($conn,$sqlCus);
					$rowCus = mysqli_fetch_assoc($resCus);

					$href = "'bill_cash_view.php?cashid=".$row['ID']."'";
					$year = date("Y",strtotime($row['Date']))+543;
					echo '<tr class="view" onclick="document.location = '.$href.';">
							<td style="text-align:center;">'.$id.'</td>
							<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
							<td>'.$rowCus['Name'].'</td>
							<td style="text-align:right;">'.number_format(round(($row['Total']/(100+$row['Vat'])*100),2), 2, '.', ',').$space.$space.$space.'</td>
							<td style="text-align:right;">'.number_format(round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2), 2, '.', ',').$space.$space.$space.'</td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>".number_format($row['Total'], 2, '.', ',').$space.$space.$space."</td>"
						."</tr>";
					$cnt++;
					$_SESSION['ID'] = $topCode;
						
					$tmp = $tmp.$sqlCus."<br>";
				}
			}
			echo	'</table>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		//////////////////////
		// Tax Invoice Page //
		//////////////////////
		case 'tax_invoice':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่บิล</th>
						<th width="80px">วันที่</th>
						<th>ชื่อลูกค้า</th>
						<th width="25%">ราคา</th>
					</tr>';

			$strSQL = "SELECT * FROM TaxInvoice";
			$topCode = 0;
			if ($result = mysqli_query($conn,$strSQL)) {
				while($row = mysqli_fetch_assoc($result)){
					if (date("Y",strtotime($row['Date'])) == date("Y")) {
						if ($topCode < $row['Code']) {
							$topCode = $row['Code'];
						}
					}
				}
			}

			$strSQL .= " ORDER BY Date DESC, ID DESC LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// ID
					$yearID = (date("y",strtotime($row['Date'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = "CH".$yearID."-";
					for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
						$id .= "0";
					}
					$id .= $row['Code'];
							
					$href = "'tax_invoice_view.php?taxinvoiceid=".$row['ID']."'";
					$year = date("Y",strtotime($row['Date']))+543;
					echo '<tr class="view" onclick="document.location = '.$href.';">
							<td style="text-align:center;">'.$id.'</td>
							<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
							<td style="text-align:center;"> เงินสด </td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>".number_format($row['Total'], 2, '.', ',').$space.$space.$space.$space.$space."</td>"
						."</tr>";
					$cnt++;
					$_SESSION['ID'] = $topCode;
					
					$tmp = $tmp.$sqlCus."<br>";
				}
			}
			echo	'</table>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		////////////////////
		// Sales Tax Page //
		////////////////////
		case 'sales_tax':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่บิล</th>
						<th width="80px">วันที่</th>
						<th>ราคา</th>
						<th>ภาษี</th>
						<th>ราคารวม</th>
					</tr>';
			$sql = "SELECT * FROM (
					    (SELECT VatCredit.ID, VatCredit.Date, VatCredit.Vat, VatCredit.Total, 'VatCredit' AS title FROM VatCredit)
					    UNION ALL
					    (SELECT VatCash.ID, VatCash.Date, VatCash.Vat, VatCash.Total, 'VatCash' AS title FROM VatCash)
					    UNION ALL
					    (SELECT TaxInvoice.ID, TaxInvoice.Date, TaxInvoice.Vat, TaxInvoice.Total, 'TaxInvoice' AS title FROM TaxInvoice)
					) results
					ORDER BY Date DESC";
			#$sql = "SELECT * FROM (VatCredit) ORDER BY Date ASC";
			#$sql .= " LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			if($result = mysqli_query($conn,$sql)){
				$cnt = 1;
				$tmp = "";
				$arrayDate = array();
				$totalnovat = 0;
				$vat = 0;
				$total = 0;
				$first = 1;
				while($row = mysqli_fetch_assoc($result)){

					$href = "'sales_tax_view.php?date=".date("Y-m",strtotime($row['Date']))."'";
					$year = date("Y",strtotime($row['Date']))+543;
					if (!in_array(date("m-",strtotime($row['Date'])).$year, $arrayDate)) {
						if ($first != 1) {
							echo'<td style="text-align:right;">'.number_format($totalnovat, 2, '.', ',').$space.$space.$space.$space.'</td>
								<td style="text-align:right;">'.number_format($vat, 2, '.', ',').$space.$space.$space.$space.'</td>'
							."<td ";
							if ($cnt%2 == 1) {
								echo "style='background-color:#B4F082;";
							}else{
								echo "style='background-color:#99E857;";
							}
							echo "text-align:right;'>".number_format($total, 2, '.', ',').$space.$space.$space.$space."</td>"
								."</tr>";
							$cnt++;
							$_SESSION['ID'] = $row['ID'];
						}else{
							$first = 0;
						}
						array_push($arrayDate, date("m-",strtotime($row['Date'])).$year);
						echo '<tr class="view" onclick="document.location = '.$href.';">
							<td style="text-align:center;">'.$cnt.'</td>
							<td style="text-align:center;">'.date("m-",strtotime($row['Date'])).$year.'</td>';
						$totalnovat = 0;
						$vat = 0;
						$total = 0;
					}else{

					}
					$totalnovat += round(($row['Total']/(100+$row['Vat'])*100),2);
					$vat += round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2);
					$total += $row['Total'];

				}
				echo'<td style="text-align:right;">'.number_format($totalnovat, 2, '.', ',').$space.$space.$space.$space.'</td>
					<td style="text-align:right;">'.number_format($vat, 2, '.', ',').$space.$space.$space.$space.'</td>'
				."<td ";
				if ($cnt%2 == 1) {
					echo "style='background-color:#B4F082;";
				}else{
					echo "style='background-color:#99E857;";
				}
				echo "text-align:right;'>".number_format($total, 2, '.', ',').$space.$space.$space.$space."</td>"
					."</tr>";
				$cnt++;
				$_SESSION['ID'] = $row['ID'];
			}
			echo	'</table>';
			// print_r($arrayDate);
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		//////////////////
		// Invoice Page //
		//////////////////
		case 'invoice':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่บิล</th>
						<th width="80px">วันที่</th>
						<th>ชื่อลูกค้า</th>
						<th width="25%">ราคา</th>
					</tr>';
			// Customer Part
			$cus = array('cnt' => 0, );
			$sqlCus = "SELECT * FROM Customer";
			if ($cusName != "") {
				$sqlCus .= " WHERE Name LIKE '%".$cusName."%'";
			}
			if ($resCus = mysqli_query($conn, $sqlCus)) {
				while($rowCus = mysqli_fetch_assoc($resCus)){
					$rowCus['cnt'] = $cus['cnt'];
					array_push($cus, $rowCus);
					$cus['cnt']++;
				}
			}
			// echo $cus['cnt'];
			// Product Part
			$pro = array('cnt' => 0, );
			$sqlPro = "SELECT * FROM Product";
			if ($proName != "") {
				$sqlPro .= " WHERE Name LIKE '%".$proName."%'";
			}
			if ($resPro = mysqli_query($conn, $sqlPro)) {
				while ($rowPro = mysqli_fetch_assoc($resPro)) {
					$rowPro['cnt'] = $pro['cnt'];
					array_push($pro, $rowPro);
					$pro['cnt']++;
				}
			}
			// echo " ".$pro['cnt'];

			$strSQL = "SELECT * FROM Invoice";
			$topCode = 0;
			if ($result = mysqli_query($conn,$strSQL)) {
				while($row = mysqli_fetch_assoc($result)){
					if (date("Y",strtotime($row['Date'])) == date("Y")) {
						if ($topCode < $row['Code']) {
							$topCode = $row['Code'];
						}
					}
				}
			}
			if ($cus['cnt'] > 0 AND $pro['cnt'] > 0) {
				$strSQL .= " WHERE";
			}
			// Where Customer in strSQL
			if ($cus['cnt'] > 0) {
				for ($i=0; $i < $cus['cnt']; $i++) { 
					if ($cus[$i]['cnt'] != 0) {
						$strSQL .= " OR CustomerID='".$cus[$i]['ID']."'";
					}else{
						$strSQL .= " (CustomerID='".$cus[$i]['ID']."'";
					}
				}
				$strSQL .= ")";
			}

			//Where Product in strSQL
			if ($pro['cnt'] > 0) {
				if ($cus['cnt'] > 0) {
					$strSQL .= " AND";
				}
				for ($i=0; $i < $pro['cnt']; $i++) { 
					if ($pro[$i]['cnt'] != 0) {
						$strSQL .= " OR LotID='".$pro[$i]['LotID']."'";
					}else{
						$strSQL .= " (LotID='".$pro[$i]['LotID']."'";
					}
				}
				$strSQL .= ")";
			}
			$strSQL .= " ORDER BY Date DESC, ID DESC LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			// echo "<br>".$strSQL;
			
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// ID
					$yearID = (date("y",strtotime($row['Date'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = "TNY".$yearID."-";
					for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
						$id .= "0";
					}
					$id .= $row['Code'];
				
					// Customer Name
					$sqlCus = "SELECT Name FROM Customer WHERE ID='".$row['CustomerID']."'";
					$resCus = mysqli_query($conn,$sqlCus);
					$rowCus = mysqli_fetch_assoc($resCus);

					$href = "'invoice_view.php?invoiceid=".$row['ID']."'";
					$year = date("Y",strtotime($row['Date']))+543;
					echo '<tr class="view" onclick="document.location = '.$href.';">
							<td style="text-align:center;">'.$id.'</td>
							<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
							<td>'.$rowCus['Name'].'</td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>".number_format($row['Total'], 2, '.', ',').$space.$space.$space.$space.$space."</td>"
						."</tr>";
					$cnt++;
					$_SESSION['ID'] = $topCode;
				}
				$tmp = $tmp.$sqlCus."<br>";
			}
			echo	'</table>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		/////////////////////////
		// Billing Credit Page //
		/////////////////////////
		case 'billing_credit':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่บิล</th>
						<th width="80px" colspan="2">วันที่</th>
						<th>ชื่อลูกค้า</th>
						<th>จำนวนบิล</th>
						<th>ราคารวม</th>
					</tr>';
			// Customer Part
			$cus = array('cnt' => 0, );
			$sqlCus = "SELECT * FROM Customer";
			if ($cusName != "") {
				$sqlCus .= " WHERE Name LIKE '%".$cusName."%'";
			}
			if ($resCus = mysqli_query($conn, $sqlCus)) {
				while($rowCus = mysqli_fetch_assoc($resCus)){
					$rowCus['cnt'] = $cus['cnt'];
					array_push($cus, $rowCus);
					$cus['cnt']++;
				}
			}
			// echo $cus['cnt'];

			$strSQL = "SELECT * FROM BillingCredit";
			$topCode = 0;
			if ($result = mysqli_query($conn,$strSQL)) {
				while($row = mysqli_fetch_assoc($result)){
					if (date("Y",strtotime($row['Date'])) == date("Y")) {
						if ($topCode < $row['Code']) {
							$topCode = $row['Code'];
						}
					}
				}
			}
			if ($cus['cnt'] > 0) {
				$strSQL .= " WHERE";
			}
			// Where Customer in strSQL
			if ($cus['cnt'] > 0) {
				for ($i=0; $i < $cus['cnt']; $i++) { 
					if ($cus[$i]['cnt'] != 0) {
						$strSQL .= " OR CustomerID='".$cus[$i]['ID']."'";
					}else{
						$strSQL .= " (CustomerID='".$cus[$i]['ID']."'";
					}
				}
				$strSQL .= ")";
			}

			$strSQL .= " ORDER BY DateEnd DESC, ID DESC LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			// echo "<br>".$strSQL;
			if($result = mysqli_query($conn,$strSQL)){
				while($row = mysqli_fetch_assoc($result)){
					// ID
					$yearID = (date("y",strtotime($row['DateEnd'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = $yearID."-";
					for ($i=0; $i < 3-strlen($row['Code']); $i++) { 
						$id .= "0";
					}
					$id .= $row['Code'];

					$href = "'billing_credit_view.php?id=".$row['ID']."'";
					$yearStart = date("Y",strtotime($row['DateStart']))+543;
					$yearEnd = date("Y",strtotime($row['DateEnd']))+543;
					//Customer
					$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
					$resCus = mysqli_query($conn,$sqlCus);
					$rowCus = mysqli_fetch_assoc($resCus);

					echo '<tr class="view" onclick="document.location = '.$href.';">
						<td style="text-align:center;">'.$id.'</td>
						<td style="text-align:center;">'.date("d/m/",strtotime($row['DateStart'])).$yearStart.'</td>
						<td style="text-align:center;">'.date("d/m/",strtotime($row['DateEnd'])).$yearEnd.'</td>
						<td>'.$rowCus['Name'].'</td>
						<td style="text-align:center;">รวม '.$row['BillCnt'].' ฉบับ</td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>".number_format($row['Total'], 2, ".", ",").$space.$space.$space.$space.$space."</td>"
						."</tr>";
					$cnt++;
					$_SESSION['ID'] = $topCode;

				}
			}
			echo	'</table>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		//////////////////////////
		// Billing Invoice Page //
		//////////////////////////
		case 'billing_invoice':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่บิล</th>
						<th width="80px" colspan="2">วันที่</th>
						<th>ชื่อลูกค้า</th>
						<th>จำนวนบิล</th>
						<th>ราคารวม</th>
					</tr>';
			// Customer Part
			$cus = array('cnt' => 0, );
			$sqlCus = "SELECT * FROM Customer";
			if ($cusName != "") {
				$sqlCus .= " WHERE Name LIKE '%".$cusName."%'";
			}
			if ($resCus = mysqli_query($conn, $sqlCus)) {
				while($rowCus = mysqli_fetch_assoc($resCus)){
					$rowCus['cnt'] = $cus['cnt'];
					array_push($cus, $rowCus);
					$cus['cnt']++;
				}
			}
			// echo $cus['cnt'];

			$strSQL = "SELECT * FROM BillingInvoice";
			$topCode = 0;
			if ($result = mysqli_query($conn,$strSQL)) {
				while($row = mysqli_fetch_assoc($result)){
					if (date("Y",strtotime($row['Date'])) == date("Y")) {
						if ($topCode < $row['Code']) {
							$topCode = $row['Code'];
						}
					}
				}
			}
			if ($cus['cnt'] > 0) {
				$strSQL .= " WHERE";
			}
			// Where Customer in strSQL
			if ($cus['cnt'] > 0) {
				for ($i=0; $i < $cus['cnt']; $i++) { 
					if ($cus[$i]['cnt'] != 0) {
						$strSQL .= " OR CustomerID='".$cus[$i]['ID']."'";
					}else{
						$strSQL .= " (CustomerID='".$cus[$i]['ID']."'";
					}
				}
				$strSQL .= ")";
			}

			$strSQL .= " ORDER BY DateEnd DESC, ID DESC LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			// echo "<br>".$strSQL;

			if($result = mysqli_query($conn,$strSQL)){
				while($row = mysqli_fetch_assoc($result)){
					// ID
					$yearID = (date("y",strtotime($row['DateEnd'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = $yearID."-";
					for ($i=0; $i < 3-strlen($row['Code']); $i++) { 
						$id .= "0";
					}
					$id .= $row['Code'];

					$href = "'billing_invoice_view.php?id=".$row['ID']."'";
					$yearStart = date("Y",strtotime($row['DateStart']))+543;
					$yearEnd = date("Y",strtotime($row['DateEnd']))+543;
					//Customer
					$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
					$resCus = mysqli_query($conn,$sqlCus);
					$rowCus = mysqli_fetch_assoc($resCus);

					echo '<tr class="view" onclick="document.location = '.$href.';">
						<td style="text-align:center;">'.$id.'</td>
						<td style="text-align:center;">'.date("d/m/",strtotime($row['DateStart'])).$yearStart.'</td>
						<td style="text-align:center;">'.date("d/m/",strtotime($row['DateEnd'])).$yearEnd.'</td>
						<td>'.$rowCus['Name'].'</td>
						<td style="text-align:center;">รวม '.$row['BillCnt'].' ฉบับ</td>'
						."<td ";
					if ($cnt%2 == 1) {
						echo "style='background-color:#B4F082;";
					}else{
						echo "style='background-color:#99E857;";
					}
					echo "text-align:right;'>".number_format($row['Total'], 2, ".", ",").$space.$space.$space.$space.$space."</td>"
						."</tr>";
					$cnt++;
					$_SESSION['ID'] = $topCode;

				}
			}
			echo	'</table>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		///////////////////
		// Customer Page //
		///////////////////
		case 'customer':
			echo '
				<table>
					<tr>
						<th width="80px">เลขที่</th>
						<th width="80px">ชื่อลูกค้า</th>
						<th>ที่อยู่ลูกค้า</th>
						<th>เครดิต</th>
						<th>เบอร์โทร</th>
						<th>เบอร์แฟกซ์</th>
						<th>สำนักงาน/สาขา</th>
						<th>เลขประจำตัวผู้เสียภาษี</th>
					</tr>';
			$strSQL = "SELECT * FROM Customer";
			if ($cusName != "") {
				$strSQL .= " WHERE Name LIKE '%".$cusName."%'";
			}
			$strSQL .= " LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			// echo '<tr><td>'.$strSQL.'</td><td>'.$cusName.'</td></tr>';
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = ""; 
				while($row = mysqli_fetch_assoc($result)){
			
					$href = "'customer_view.php?customerid=".$row['ID']."'";
					echo '<tr class="view" onclick="document.location = '.$href.';">
							<td style="text-align:center;">'.$row['ID'].'</td>
							<td style="text-align:center;">'.$row['Name'].'</td>
							<td>'.$row['Address'].'</td>
							<td style="text-align:center;">'.$row['Credit'].'</td>
							<td style="text-align:center;">'.$row['Tel'].'</td>
							<td style="text-align:center;">'.$row['Fax'].'</td>
							<td style="text-align:center;">'.$row['IsMain'];
					// Main or Branch xx
					if ($row['IsMain'] == 'สาขาที่') {
						echo ' ';
						$len = 5-strlen($row['Branch']);
						for ($i=0; $i < $len; $i++) { 
							echo '0';
						}
						echo $row['Branch'];
					}
					echo	'</td>
							<td style="text-align:center;">'.$row['TaxID'].'</td>'
						."</tr>";
					$cnt++;
				}
			}
			echo	'</table>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		default:
			
			break;
	}
?>