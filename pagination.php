<?php
	session_start();
	include("connection.php");
	if (isset($_GET['pagination'])) {
		$_SESSION['orderlistpage'] = $_GET['pagination'];
		// echo "orderlistpage : ".$_SESSION['orderlistpage'];
	}
	if (isset($_GET['page'])) {
		$_SESSION['orderlist'] = $_GET['page'];
		// echo "  orderlist : ".$_SESSION['orderlist'];
	}

	if (isset($_GET['cusName'])) {
		$_SESSION['cusName'] = $_GET['cusName'];
		$cusName = $_GET['cusName'];
	}elseif (isset($_SESSION['cusName'])) {
		$cusName = $_SESSION['cusName'];
	}

	if (isset($_GET['proName'])) {
		$_SESSION['proName'] = $_GET['proName'];
		$proName = $_GET['proName'];
	}elseif (isset($_SESSION['proName'])) {
		$proName = $_SESSION['proName'];
	}

	switch ($_SESSION['orderlist']) {
		/////////////////////
		// Vat Credit Page //
		/////////////////////
		case 'credit':
			echo '
				<a class="btn margin-b smooth" href="bill_credit_add.php">เพิ่มบิล</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อลูกค้า <input type="text" value="'.$_SESSION['cusName'].'" onkeyup="SearchByCustomerName(this.value,\'credit\')"/>
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อสินค้า <input type="text" value="'.$_SESSION['proName'].'" onkeyup="SearchByProductName(this.value,\'credit\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$cnt = 0;
			$strSQL = "SELECT * FROM VatCredit";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// Customer Part
					$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
					if ($cusName != "") {
						$sqlCus .= " AND Name LIKE '%".$cusName."%'";
					}
					if ($resCus = mysqli_query($conn, $sqlCus)) {
						$haveCus = 0;
						while($rowCus = mysqli_fetch_assoc($resCus)){
							$haveCus = 1;
						}
					}
					// Product Part
					$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
					if ($proName != "") {
						$sqlPro .= " AND Name LIKE '%".$proName."%'";
					}
					if ($resPro = mysqli_query($conn, $sqlPro)) {
						$haveProduct = 0;
						while ($rowPro = mysqli_fetch_assoc($resPro)) {
							$haveProduct = 1;
						}
					}
					if ($haveProduct == 1 AND $haveCus == 1) {
						$cnt++;
					}
				}
			}
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo'<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		///////////////////
		// Vat Cash Page //
		///////////////////
		case 'cash':
			echo '
				<a class="btn margin-b smooth" href="bill_cash_add.php">เพิ่มบิล</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อลูกค้า <input type="text" value="'.$_SESSION['cusName'].'" onkeyup="SearchByCustomerName(this.value,\'cash\')"/>
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อสินค้า <input type="text" value="'.$_SESSION['proName'].'" onkeyup="SearchByProductName(this.value,\'cash\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$cnt = 0;
			$strSQL = "SELECT * FROM VatCash";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// Customer Part
					$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
					if ($cusName != "") {
						$sqlCus .= " AND Name LIKE '%".$cusName."%'";
					}
					if ($resCus = mysqli_query($conn, $sqlCus)) {
						$haveCus = 0;
						while($rowCus = mysqli_fetch_assoc($resCus)){
							$haveCus = 1;
						}
					}
					// Product Part
					$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
					if ($proName != "") {
						$sqlPro .= " AND Name LIKE '%".$proName."%'";
					}
					if ($resPro = mysqli_query($conn, $sqlPro)) {
						$haveProduct = 0;
						while ($rowPro = mysqli_fetch_assoc($resPro)) {
							$haveProduct = 1;
						}
					}
					if ($haveProduct == 1 AND $haveCus == 1) {
						$cnt++;
					}
				}
			}
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo'<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		//////////////////////
		// Tax Invoice Page //
		//////////////////////
		case 'tax_invoice':
			echo '
				<a class="btn margin-b smooth" href="tax_invoice_add.php">เพิ่มบิล</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อสินค้า <input type="text" value="'.$_SESSION['proName'].'" onkeyup="SearchByProductName(this.value,\'tax_invoice\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$cnt = 0;
			$strSQL = "SELECT * FROM TaxInvoice";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// Product Part
					$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
					if ($proName != "") {
						$sqlPro .= " AND Name LIKE '%".$proName."%'";
					}
					if ($resPro = mysqli_query($conn, $sqlPro)) {
						$haveProduct = 0;
						while ($rowPro = mysqli_fetch_assoc($resPro)) {
							$cnt++;
						}
					}
				}
			}
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo'<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		////////////////////
		// Sales Tax Page //
		////////////////////
		case 'sales_tax':
			echo '
				<a class="btn margin-b smooth" href="tax_invoice_add.php">เพิ่มบิล</a>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$sql = "SELECT * FROM (
					    (SELECT VatCredit.ID, VatCredit.Date, VatCredit.Vat, VatCredit.Total, 'VatCredit' AS title FROM VatCredit)
					    UNION ALL
					    (SELECT VatCash.ID, VatCash.Date, VatCash.Vat, VatCash.Total, 'VatCash' AS title FROM VatCash)
					    UNION ALL
					    (SELECT TaxInvoice.ID, TaxInvoice.Date, TaxInvoice.Vat, TaxInvoice.Total, 'TaxInvoice' AS title FROM TaxInvoice)
					) results
					ORDER BY Date DESC";
			#$sql = "SELECT * FROM (VatCredit) ORDER BY Date ASC";
			#$sql .= " LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
			if($result = mysqli_query($conn,$sql)){
				$cnt = 1;
				$first = 1;
				$arrayDate = array();
				while($row = mysqli_fetch_assoc($result)){
					if (!in_array(date("m-Y",strtotime($row['Date'])), $arrayDate)) {
						if ($first != 1) {
							$cnt++;
						}else{
							$first = 0;
						}
						array_push($arrayDate, date("m-Y",strtotime($row['Date'])));
					}
				}
				$cnt++;
			}
			echo	'</table>';
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo'<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		//////////////////
		// Invoice Page //
		//////////////////
		case 'invoice':
			echo '
				<a class="btn margin-b smooth" href="invoice_add.php">เพิ่มบิล</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อลูกค้า <input type="text" value="'.$_SESSION['cusName'].'" onkeyup="SearchByCustomerName(this.value,\'invoice\')"/>
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อสินค้า <input type="text" value="'.$_SESSION['proName'].'" onkeyup="SearchByProductName(this.value,\'invoice\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$cnt = 0;
			$strSQL = "SELECT * FROM Invoice";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// Product Part
					$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
					if ($proName != "") {
						$sqlPro .= " AND Name LIKE '%".$proName."%'";
					}
					if ($resPro = mysqli_query($conn, $sqlPro)) {
						$haveProduct = 0;
						while ($rowPro = mysqli_fetch_assoc($resPro)) {
							$cnt++;
						}
					}
				}
			}
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo'<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		/////////////////////////
		// Billing Credit Page //
		/////////////////////////
		case 'billing_credit':
			echo '
				<a class="btn margin-b smooth" href="billing_credit_add.php">เพิ่มใบวางบิล</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อลูกค้า <input type="text" value="'.$_SESSION['cusName'].'" onkeyup="SearchByCustomerName(this.value,\'billing_credit\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$cnt = 0;
			$strSQL = "SELECT * FROM BillingCredit";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// Customer Part
					$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
					if ($cusName != "") {
						$sqlCus .= " AND Name LIKE '%".$cusName."%'";
					}
					if ($resCus = mysqli_query($conn, $sqlCus)) {
						$haveCus = 0;
						while($rowCus = mysqli_fetch_assoc($resCus)){
							$cnt++;
						}
					}
				}
			}
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo'<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#
		
		//////////////////////////
		// Billing Invoice Page //
		//////////////////////////
		case 'billing_invoice':
			echo '
				<a class="btn margin-b smooth" href="billing_invoice_add.php">เพิ่มใบวางบิล</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อลูกค้า <input type="text" value="'.$_SESSION['cusName'].'" onkeyup="SearchByCustomerName(this.value,\'billing_invoice\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$cnt = 0;
			$strSQL = "SELECT * FROM Invoice";
			if($result = mysqli_query($conn,$strSQL)){
				$cnt = 1;
				$tmp = "";
				while($row = mysqli_fetch_assoc($result)){
					// Customer Part
					$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
					if ($cusName != "") {
						$sqlCus .= " AND Name LIKE '%".$cusName."%'";
					}
					if ($resCus = mysqli_query($conn, $sqlCus)) {
						$haveCus = 0;
						while($rowCus = mysqli_fetch_assoc($resCus)){
							$cnt++;
						}
					}
				}
			}
			// echo $cnt;

			if ($_SESSION['orderlistpage'] <= 3) {
				echo '<li class="active"><a href="#"> |< </a></li>';
				$i=0;
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $cnt/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#

		///////////////////
		// Customer Page //
		///////////////////
		case 'customer':
			echo '
				<a class="btn margin-b smooth" href="customer_add.php">เพิ่มลูกค้า</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ค้นหา
				&nbsp;&nbsp;&nbsp;&nbsp;ชื่อลูกค้า <input type="text" value="'.$_SESSION['cusName'].'" onkeyup="SearchByCustomerName(this.value,\'customer\')"/>
				<ul class="pagination float-r">
					<li class="active"><a>หน้า</a></li>';
			$sql = "SELECT COUNT(*) AS Total FROM Customer";
			if ($cusName != "") {
				$sql .= " WHERE Name LIKE '%".$cusName."%'";
			}
			// echo $sql;
			$result = mysqli_query($conn, $sql);
			$data = mysqli_fetch_assoc($result);
			// echo $data['Total'];
			if ($_SESSION['orderlistpage'] <= 3) {
				$i=0;
				echo'<li class="active"><a href="#"> |< </a></li>';
			}else{
				$i = $_SESSION['orderlistpage']-3;
			}
			if ($_SESSION['orderlistpage'] > 3) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']-1).'\',\''.$_SESSION['orderlist'].'\')"> << </a></li>';
			}
			for ($j=0 ; $i <= $data['Total']/20 && $j < 5; $i++) { 
				echo'<li';
				if ($_SESSION['orderlistpage'] == $i+1) {
					echo ' class="active"';
				}
				echo'><a href="#" onclick="Pagination(\''.($i+1).'\',\''.$_SESSION['orderlist'].'\')">'.($i+1).'</a></li>';
				$j++;
			}
			if ($i >= 5 && $_SESSION['orderlistpage'] != $i) {
				echo'<li><a href="#" onclick="Pagination(\''.($_SESSION['orderlistpage']+1).'\',\''.$_SESSION['orderlist'].'\')"> >> </a></li>';
			}else{
				echo'<li class="active"><a href="#"> >| </a></li>';
			}
			echo '</ul>';
			break;
		#-------------------------------------------------------------------------------------------------------------------------------------#
		
		default:
			# code...
			break;
	}
?>