	<?php
	require('fpdf.php');
	include("connection.php");
	include("ThaiBaht.php");

	if (isset($_GET['type'])) {
		$type = $_GET['type'];
	}

	// Convert a string to an array with multibyte string
	function getMBStrSplit($string, $split_length = 1){
		mb_internal_encoding('UTF-8');
		mb_regex_encoding('UTF-8'); 
		
		$split_length = ($split_length <= 0) ? 1 : $split_length;
		$mb_strlen = mb_strlen($string, 'utf-8');
		$array = array();
		$i = 0; 
		
		while($i < $mb_strlen)
		{
			$array[] = mb_substr($string, $i, $split_length);
			$i = $i+$split_length;
		}
		
		return $array;
	}
	// Get string length for Character Thai
	function getStrLenTH($string){
		$array = getMBStrSplit($string);
		$count = 0;
		
		foreach($array as $value)
		{
			$ascii = ord(iconv("UTF-8", "TIS-620", $value ));
			
			if( !( $ascii == 209 ||  ($ascii >= 212 && $ascii <= 218 ) || ($ascii >= 231 && $ascii <= 238 )) )
			{
				$count += 1;
			}
		}
		return $count;
	}

	if (isset($type)) {
		switch ($type) {
			#-------------------------------------------------------------------------------------------------------------------------------------#

			//////////////////////
			// Bill Credit Page //
			//////////////////////
			case 'bill_credit':
				$sql = "SELECT * FROM VatCredit WHERE ID='".$_GET['id']."'";
				$result = mysqli_query($conn,$sql);
				$row = mysqli_fetch_assoc($result);
				$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
				$resultCus = mysqli_query($conn,$sqlCus);
				$rowCus = mysqli_fetch_assoc($resultCus);

				// ID
				$yearID = (date("y",strtotime($row['Date'])))+43;
				if ($yearID > 100) {
					$yearID -= 100;
				}
				$id = "IV".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
					$id .= "0";
				}
				$id .= $row['Code'];

				$ID = $id;
				$Date = date('d/m/').(date('Y')+543);
				$CustomerName = $rowCus['Name'];
				$CustomerAddress = $rowCus['Address'];
				$CustomerTaxID = $rowCus['TaxID'];
				$PO = $row['PO'];
				$Deadline = date('d/m/',strtotime($row['Deadline'])).(date('Y',strtotime($row['Deadline']))+543);
				$VatPercent = $row['Vat'];
				$BillCnt = $row['BillCnt'];

				$pdf = new FPDF();
				$pdf->AddPage('P','Letter');
				$pdf->AddFont('tahoma');
				$pdf->AddFont('tahomab');
				$YBegin = 46;
				$XBegin = 17;
				// เล่มบิล
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin+153,$YBegin-1);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$ID),0,1,"L");
				// วันที่
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin+108,$YBegin-1);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"L");
				// PO & Deadline
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin+123,$YBegin+11);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$PO),0,0,"L");
				$pdf->SetXY($XBegin+118,$YBegin+23);
				$pdf->Cell(20,7,iconv( 'UTF-8','TIS-620',$Deadline),0,0,"L");
				$pdf->SetX($XBegin+167);
				$pdf->Cell(20,7,iconv( 'UTF-8','TIS-620',$rowCus['Credit']." วัน"),0,0,"L");
				// Customer Name
				$pdf->SetXY($XBegin,$YBegin-1);
				$pdf->SetFont('tahoma','',10);
				$pdf->MultiCell(90,5,iconv( 'UTF-8','TIS-620',$CustomerName),0);
				$pdf->SetXY($XBegin,$YBegin+9);
				$pdf->MultiCell(90,6,iconv( 'UTF-8','TIS-620',$CustomerAddress),0);
				$pdf->SetXY($XBegin+28,$YBegin+22);
				$pdf->Cell(62,7,iconv( 'UTF-8','TIS-620',$CustomerTaxID),0,1,"L");
				// // Table Head
				// $pdf->SetXY(15,57);
				// $pdf->SetFont('tahomab','',12);
				// $pdf->Cell(15,7,iconv( 'UTF-8','TIS-620','ลำดับ'),0,0,"C");
				// $pdf->Cell(80,7,iconv( 'UTF-8','TIS-620','รายการ'),0,0,"L");
				// $pdf->Cell(20,7,iconv( 'UTF-8','TIS-620','จำนวน'),0,0,"C");
				// $pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','ราคา'),0,0,"C");
				// $pdf->Cell(40,7,iconv( 'UTF-8','TIS-620','จำนวนเงิน'),0,0,"C");
				// Bill Table
				$pdf->SetFont('tahoma','',10);
				$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
				// echo $sqlPro." ".$i."<br>";
				if($resultPro = mysqli_query($conn,$sqlPro)){
					$cnt = 1;
					$textwrap = 0;
					$total = 0;
					$YBegin = 82;
					while ($rowPro = mysqli_fetch_assoc($resultPro)) {
						$quantityInt = "";
						$quantityPlace = "";
						if ($rowPro['Quantity']-floor($rowPro['Quantity']) == 0) {
							$quantityInt = $rowPro['Quantity'];
						}else{
							$tmp = explode(".", $rowPro['Quantity']);
							$quantityInt = $tmp[0].".";
							$quantityPlace = $tmp[1];
						}
						$pdf->SetXY(0,($YBegin+(($cnt+$textwrap)*7)));
						$pdf->Cell(12,7,iconv( 'UTF-8','TIS-620',$cnt),0,0,"C");
						$pdf->Cell(14,7,iconv( 'UTF-8','TIS-620',""),0,0,"C");
						$pdf->MultiCell(100,7,iconv( 'UTF-8','TIS-620',"  ".$rowPro['Name']),0);
						$pdf->SetXY(0+12+14+100,($YBegin+(($cnt+$textwrap)*7)));
						// $pdf->Cell(8,7,iconv( 'UTF-8','TIS-620',$quantityInt),0,0,"R");
						// $pdf->SetXY(0+12+14+100+6,($YBegin+(($cnt+$textwrap)*7)));
						// $pdf->Cell(2,7,iconv( 'UTF-8','TIS-620',$quantityPlace),0,0,"L");
						// $pdf->SetXY(0+12+14+100+9,($YBegin+(($cnt+$textwrap)*7)));
						// $pdf->SetFont('tahoma','',9);
						$pdf->Cell(19,7,iconv( 'UTF-8','TIS-620',$rowPro['Quantity']." ".$rowPro['Unit']),0,0,"L");
						$pdf->SetFont('tahoma','',10);
						$pdf->Cell(18,7,iconv( 'UTF-8','TIS-620',number_format($rowPro['PriceEach'], 2, '.', ',')),0,0,"R");
						$pdf->SetX(166);
						$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format(($rowPro['PriceEach']*$rowPro['Quantity']), 2, '.', ',')),0,0,"R");
						$cnt++;
						if (getStrLenTH($rowPro['Name']) > 55) {
							$textwrap++;
						}
						$total += $rowPro['PriceEach']*$rowPro['Quantity'];

					}
				}
				// Table Total
				$yStart = 200;
				$xStart = 166;
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($xStart,$yStart);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total, 2, '.', ',')),0,0,"R");
				$pdf->SetXY($xStart,$yStart+11);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total*($VatPercent)/100, 2, '.', ',')),0,0,"R");
				$pdf->SetXY(0,$yStart+22);
				$pdf->Cell(126,7,iconv( 'UTF-8','TIS-620','('.ThaiBahtConversion($total).')'),0,0,"C");
				$pdf->SetXY($xStart,$yStart+22);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total*(100+$VatPercent)/100, 2, '.', ',')),0,0,"R");
				// Output
				$pdf->Output();
				break;
			#-------------------------------------------------------------------------------------------------------------------------------------#

			////////////////////
			// Bill Cash Page //
			////////////////////
			case 'bill_cash':
				$sql = "SELECT * FROM VatCash WHERE ID='".$_GET['id']."'";
				$result = mysqli_query($conn,$sql);
				$row = mysqli_fetch_assoc($result);
				$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
				$resultCus = mysqli_query($conn,$sqlCus);
				$rowCus = mysqli_fetch_assoc($resultCus);

				// ID
				$yearID = (date("y",strtotime($row['Date'])))+43;
				if ($yearID > 100) {
					$yearID -= 100;
				}
				$id = "CR".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
					$id .= "0";
				}
				$id .= $row['Code'];

				$ID = $id;
				$Date = date('d/m/').(date('Y')+543);
				$CustomerName = $rowCus['Name'];
				$CustomerAddress = $rowCus['Address'];
				$CustomerTaxID = $rowCus['TaxID'];
				$PO = $row['PO'];
				$VatPercent = $row['Vat'];
				$BillCnt = $row['BillCnt'];

				$pdf = new FPDF();
				$pdf->AddPage('P','Letter');
				$pdf->AddFont('tahoma');
				$pdf->AddFont('tahomab');
				$YBegin = 48;
				$XBegin = 17;
				// เล่มบิล
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin+146,$YBegin+5);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$ID),0,1,"L");
				// วันที่
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin+146,$YBegin+18);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"L");
				// Customer Name
				$pdf->SetXY($XBegin,$YBegin+1);
				$pdf->SetFont('tahoma','',10);
				$pdf->MultiCell(126,5,iconv( 'UTF-8','TIS-620',$CustomerName),0,"L");
				$pdf->SetXY($XBegin,$YBegin+11);
				$pdf->MultiCell(110,6,iconv( 'UTF-8','TIS-620',$CustomerAddress),0);
				$pdf->SetXY($XBegin+28,$YBegin+22);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$CustomerTaxID),0,1,"L");
				// // Table Head
				// $pdf->SetXY(15,57);
				// $pdf->SetFont('tahomab','',12);
				// $pdf->Cell(15,7,iconv( 'UTF-8','TIS-620','ลำดับ'),0,0,"C");
				// $pdf->Cell(80,7,iconv( 'UTF-8','TIS-620','รายการ'),0,0,"L");
				// $pdf->Cell(20,7,iconv( 'UTF-8','TIS-620','จำนวน'),0,0,"C");
				// $pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','ราคา'),0,0,"C");
				// $pdf->Cell(40,7,iconv( 'UTF-8','TIS-620','จำนวนเงิน'),0,0,"C");
				// Bill Table
				$pdf->SetFont('tahoma','',10);
				$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
				// echo $sqlPro." ".$i."<br>";
				if($resultPro = mysqli_query($conn,$sqlPro)){
					$cnt = 1;
					$textwrap = 0;
					$total = 0;
					$YBegin = 82;
					while ($rowPro = mysqli_fetch_assoc($resultPro)) {
						$quantityInt = "";
						$quantityPlace = "";
						if ($rowPro['Quantity']-floor($rowPro['Quantity']) == 0) {
							$quantityInt = $rowPro['Quantity'];
						}else{
							$tmp = explode(".", $rowPro['Quantity']);
							$quantityInt = $tmp[0].".";
							$quantityPlace = $tmp[1];
						}
						$pdf->SetXY(1,($YBegin+(($cnt+$textwrap)*7)));
						$pdf->Cell(10,7,iconv( 'UTF-8','TIS-620',$cnt),0,0,"C");
						// $pdf->Cell(0,7,iconv( 'UTF-8','TIS-620',""),0,0,"C");
						$pdf->MultiCell(110,7,iconv( 'UTF-8','TIS-620',$rowPro['Name']),0);
						$pdf->SetXY(1+12+14+100,($YBegin+(($cnt+$textwrap)*7)));
						// $pdf->Cell(8,7,iconv( 'UTF-8','TIS-620',$quantityInt),0,0,"R");
						// $pdf->SetXY(1+12+14+100+6,($YBegin+(($cnt+$textwrap)*7)));
						// $pdf->Cell(2,7,iconv( 'UTF-8','TIS-620',$quantityPlace),0,0,"L");
						// $pdf->SetXY(1+12+14+100+9,($YBegin+(($cnt+$textwrap)*7)));
						$pdf->Cell(19,7,iconv( 'UTF-8','TIS-620',$rowPro['Quantity']." ".$rowPro['Unit']),0,0,"L");
						$pdf->Cell(17,7,iconv( 'UTF-8','TIS-620',number_format($rowPro['PriceEach'], 2, '.', ',')),0,0,"R");
						$pdf->SetX(166);
						$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format(($rowPro['PriceEach']*$rowPro['Quantity']), 2, '.', ',')),0,0,"R");
						$cnt++;
						if (getStrLenTH($rowPro['Name']) > 98) {
							$textwrap++;
						}
						$total += $rowPro['PriceEach']*$rowPro['Quantity'];

					}
				}
				// Table Total
				$YBegin = 216;
				$XBegin = 166;
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin,$YBegin);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total, 2, '.', ',')),0,0,"R");
				$pdf->SetXY($XBegin,$YBegin+11);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total*($VatPercent)/100, 2, '.', ',')),0,0,"R");
				$pdf->SetXY(1,$YBegin+23);
				$pdf->Cell(126,7,iconv( 'UTF-8','TIS-620','('.ThaiBahtConversion($total).')'),0,0,"C");
				$pdf->SetXY($XBegin,$YBegin+22);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total*(100+$VatPercent)/100, 2, '.', ',')),0,0,"R");
				// Output
				$pdf->Output();
				break;
			#-------------------------------------------------------------------------------------------------------------------------------------#

			//////////////////////
			// Tax Invoice Page //
			//////////////////////
			case 'tax_invoice':
				$sql = "SELECT * FROM TaxInvoice WHERE ID='".$_GET['id']."'";
				$result = mysqli_query($conn,$sql);
				$row = mysqli_fetch_assoc($result);

				// ID
				$yearID = (date("y",strtotime($row['Date'])))+43;
				if ($yearID > 100) {
					$yearID -= 100;
				}
				$id = "CH".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
					$id .= "0";
				}
				$id .= $row['Code'];

				$ID = $id;
				$Date = date('d/m/').(date('Y')+543);
				$CustomerName = 'เงินสด';
				$BillCnt = $row['BillCnt'];

				$pdf = new FPDF();
				$pdf->AddPage('P','Letter');
				$pdf->AddFont('tahoma');
				$pdf->AddFont('tahomab');
				$XBegin = 30;
				$YBegin = 23;
				// $pdf->Cell(1,0,"asdf");
				//ใบวางบิล
				$pdf->SetXY($XBegin+65,$YBegin-10);
				$pdf->SetFont('tahomab','',15);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ'),0,1,"R");
				// Default
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY($XBegin,$YBegin);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ไทยนิยมฮาร์ดแวร์'),0,1,"L");
				$pdf->SetX($XBegin);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','856/11-12  ถ.เจตน์จำนง  ต.บางปลาสร้อย'),0,1,"L");
				$pdf->SetX($XBegin);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','อ.เมืองชลบุรี  จ.ชลบุรี  20000'),0,1,"L");
				$pdf->SetX($XBegin);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','Tel.(038) 273507-8   Fax.(038) 273507'),0,1,"L");
				$pdf->SetX($XBegin);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เลขประจำตัวผู้เสียภาษี   3 2099 00478 61 7'),0,1,"L");
				// เล่มบิล
				$pdf->SetXY($XBegin+125,$YBegin);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เล่มที่'),0,0,"L");
				$pdf->SetX($XBegin+140);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$ID),0,1,"L");
				// วันที่
				$pdf->SetXY($XBegin+125,$YBegin+21);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','วันที่'),0,0,"L");
				$pdf->SetX($XBegin+140);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"L");
				// Customer Name
				$pdf->SetXY($XBegin,$YBegin+35);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','นาม : '.$CustomerName),0,1,"L");
				// Table Head
				$pdf->SetXY($XBegin-15,$YBegin+42);
				$pdf->SetFont('tahomab','',9);
				$pdf->Cell(15,7,iconv( 'UTF-8','TIS-620','ลำดับ'),0,0,"C");
				$pdf->Cell(80,7,iconv( 'UTF-8','TIS-620','รายการ'),0,0,"L");
				$pdf->Cell(20,7,iconv( 'UTF-8','TIS-620','จำนวน'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','ราคา'),0,0,"C");
				$pdf->Cell(40,7,iconv( 'UTF-8','TIS-620','จำนวนเงิน'),0,0,"C");
				// Bill Table
				$pdf->SetFont('tahoma','',10);
				$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
				// echo $sqlPro." ".$i."<br>";
				if($resultPro = mysqli_query($conn,$sqlPro)){
					$cnt = 1;
					$total = 0;
					while ($rowPro = mysqli_fetch_assoc($resultPro)) {
						$pdf->SetXY($XBegin-15,($YBegin+42+($cnt*7)));
						$pdf->Cell(15,7,iconv( 'UTF-8','TIS-620',$cnt),0,0,"C");
						$pdf->Cell(80,7,iconv( 'UTF-8','TIS-620',$rowPro['Name']),0,0,"L");
						$pdf->Cell(20,7,iconv( 'UTF-8','TIS-620',$rowPro['Quantity']." ".$rowPro['Unit']),0,0,"C");
						$pdf->SetXY(140,($YBegin+42+($cnt*7)));
						$pdf->Cell(10,7,iconv( 'UTF-8','TIS-620',number_format($rowPro['PriceEach'], 2, '.', ',')),0,0,"R");
						$pdf->SetXY(160,($YBegin+42+($cnt*7)));
						$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format(($rowPro['PriceEach']*$rowPro['Quantity']), 2, '.', ',')),0,0,"R");
						$cnt++;
						$total += $rowPro['PriceEach']*$rowPro['Quantity'];
					}
				}
				// Table Total
				$pdf->SetXY($XBegin-15,$YBegin+85);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(110,7,iconv( 'UTF-8','TIS-620','('.ThaiBahtConversion($total).')'),0,0,"C");
				$pdf->SetXY($XBegin+80,$YBegin+85);
				$pdf->Cell(50,7,iconv( 'UTF-8','TIS-620','(รวมภาษีมูลค่าเพิ่มแล้ว) รวมเงิน'),0,0,"C");
				$pdf->SetXY($XBegin+130,$YBegin+85);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total, 2, '.', ',')),0,0,"R");
				$pdf->SetXY($XBegin+135,$YBegin+85);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',''),'B');
				$pdf->SetXY($XBegin+135,$YBegin+86);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',''),'B');
				// Signature
				$pdf->SetXY($XBegin+50,$YBegin+98);
				$pdf->Cell(40,7,iconv( 'UTF-8','TIS-620','ลงชื่อ'),0,0,"R");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620',''),'B',0,"R");
				$pdf->SetXY($XBegin+90,$YBegin+105);
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้รับเงิน'),0,0,"C");
				// Output
				$pdf->Output();
				break;
			#-------------------------------------------------------------------------------------------------------------------------------------#

			//////////////////
			// Invoice Page //
			//////////////////
			case 'invoice':
				$sql = "SELECT * FROM Invoice WHERE ID='".$_GET['id']."'";
				$result = mysqli_query($conn,$sql);
				$row = mysqli_fetch_assoc($result);
				$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
				$resultCus = mysqli_query($conn,$sqlCus);
				$rowCus = mysqli_fetch_assoc($resultCus);

				// ID
				$yearID = (date("y",strtotime($row['Date'])))+43;
				if ($yearID > 100) {
					$yearID -= 100;
				}
				$id = "TNY".$yearID."-";
				for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
					$id .= "0";
				}
				$id .= $row['Code'];

				$ID = $id;
				$Date = date('d/m/').(date('Y')+543);
				$CustomerName = $rowCus['Name'];
				$BillCnt = $row['BillCnt'];

				$pdf = new FPDF();
				$pdf->AddPage('P','Letter');
				$pdf->AddFont('tahoma');
				$pdf->AddFont('tahomab');
				// $pdf->Cell(1,0,"asdf");
				//ใบวางบิล
				$pdf->SetXY(95,18);
				$pdf->SetFont('tahomab','',25);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ใบส่งของ'),0,1,"L");
				// Default
				$pdf->SetFont('tahomab','',25);
				$pdf->SetXY(25,18);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','TNY'),0,1,"L");
				// เล่มบิล
				$pdf->SetXY(155,15);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เล่มที่'),0,0,"L");
				$pdf->SetX(170);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$ID),0,1,"L");
				// วันที่
				$pdf->SetXY(155,22);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','วันที่'),0,0,"L");
				$pdf->SetX(170);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"L");
				// Customer Name
				$pdf->SetXY(15,30);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ลูกค้า : '.$CustomerName),0,1,"L");
				// Table Head
				$pdf->SetXY(15,37);
				$pdf->SetFont('tahomab','',9);
				$pdf->Cell(15,7,iconv( 'UTF-8','TIS-620','ลำดับ'),0,0,"C");
				$pdf->Cell(80,7,iconv( 'UTF-8','TIS-620','รายการ'),0,0,"C");
				$pdf->Cell(20,7,iconv( 'UTF-8','TIS-620','จำนวน'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','ราคา'),0,0,"C");
				$pdf->Cell(40,7,iconv( 'UTF-8','TIS-620','จำนวนเงิน'),0,0,"C");
				// Bill Table
				$pdf->SetFont('tahoma','',10);
				$sqlPro = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
				// echo $sqlPro." ".$i."<br>";
				if($resultPro = mysqli_query($conn,$sqlPro)){
					$cnt = 1;
					$total = 0;
					while ($rowPro = mysqli_fetch_assoc($resultPro)) {
						$pdf->SetXY(15,(37+($cnt*7)));
						$pdf->Cell(15,7,iconv( 'UTF-8','TIS-620',$cnt),0,0,"C");
						$pdf->Cell(80,7,iconv( 'UTF-8','TIS-620',$rowPro['Name']),0,0,"L");
						$pdf->Cell(20,7,iconv( 'UTF-8','TIS-620',$rowPro['Quantity']." ".$rowPro['Unit']),0,0,"C");
						$pdf->SetXY(140,(37+($cnt*7)));
						$pdf->Cell(10,7,iconv( 'UTF-8','TIS-620',number_format($rowPro['PriceEach'], 2, '.', ',')),0,0,"R");
						$pdf->SetXY(160,(37+($cnt*7)));
						$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format(($rowPro['PriceEach']*$rowPro['Quantity']), 2, '.', ',')),0,0,"R");
						$cnt++;
						$total += $rowPro['PriceEach']*$rowPro['Quantity'];
					}
				}
				// Table Total
				$pdf->SetXY(15,100);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(145,7,iconv( 'UTF-8','TIS-620','('.ThaiBahtConversion($total).')'),0,0,"C");
				$pdf->SetXY(160,100);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($total, 2, '.', ',')),0,0,"R");
				$pdf->SetXY(165,100);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',''),'B');
				$pdf->SetXY(165,101);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',''),'B');
				// Signature
				$pdf->SetXY(15,113);
				$pdf->Cell(10,7,iconv( 'UTF-8','TIS-620','ลงชื่อ'),0,0,"R");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620',''),'B',0,"L");
				$pdf->Cell(40,7,iconv( 'UTF-8','TIS-620','ลงชื่อ'),0,0,"R");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620',''),'B',0,"R");
				$pdf->SetXY(25,120);
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้รับสินค้า'),0,0,"C");
				$pdf->Cell(40,7,iconv( 'UTF-8','TIS-620',''),0,0,"C");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้ส่งของ'),0,0,"C");
				// Output
				$pdf->Output();
				break;
			#-------------------------------------------------------------------------------------------------------------------------------------#

			/////////////////////////
			// Billing Credit Page //
			/////////////////////////
			case 'billing_credit':
				$sql = "SELECT * FROM BillingCredit WHERE ID='".$_GET['id']."'";
				$result = mysqli_query($conn,$sql);
				$row = mysqli_fetch_assoc($result);
				$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
				$resultCus = mysqli_query($conn,$sqlCus);
				$rowCus = mysqli_fetch_assoc($resultCus);
				$billList = explode("-", $row['BillList']);

				$yearID = (date("y",strtotime($row['DateEnd'])))+43;
				if ($yearID > 100) {
					$yearID -= 100;
				}
				$id = '';
				for ($j=0; $j < 3-strlen($row['Code']); $j++) { 
					$id .= "0";
				}
				$id .= $row['Code'];

				$bookID = $yearID;
				$ID = $id;
				$Date = date('d/m/').(date('Y')+543);
				$CustomerName = $rowCus['Name'];
				$BillCnt = $row['BillCnt'];

				$pdf = new FPDF();
				$pdf->SetAutoPageBreak(false);
				$pdf->AddPage('P','Letter');
				$tmpLenght = 0;
				if ($BillCnt > 9) {
					$tmpLenght = 135;
				}
				$pdf->AddFont('tahoma');
				$pdf->AddFont('tahomab');

				// $pdf->Cell(1,0,"asdf");
				//ใบวางบิล
				$pdf->SetXY(93,18);
				$pdf->SetFont('tahomab','',15);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ใบวางบิล'),0,1,"L");
				// Default
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY(30,15);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ไทยนิยมฮาร์ดแวร์'),0,1,"L");
				$pdf->SetX(30);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','856/11-12  ถ.เจตน์จำนง  ต.บางปลาสร้อย'),0,1,"L");
				$pdf->SetX(30);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','อ.เมืองชลบุรี  จ.ชลบุรี  20000       Tel.(038) 273507-8   Fax.(038) 273507'),0,1,"L");
				// เล่มบิล
				$pdf->SetXY(155,15);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เล่มที่'),0,0,"L");
				$pdf->SetX(170);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$bookID),0,1,"L");
				// เล่มที่
				$pdf->SetXY(155,22);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เล่มที่'),0,0,"L");
				$pdf->SetX(170);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$ID),0,0,"L");
				// วันที่
				$pdf->SetXY(145,29);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','วันที่'),0,0,"L");
				$pdf->SetX(155);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"L");
				// Customer Name
				$pdf->SetXY(30,36);
				$pdf->SetFont('tahomab','',11);
				$pdf->Cell(125,7,iconv( 'UTF-8','TIS-620','  '.$CustomerName),1,1,"L");
				// Table Head
				$pdf->SetXY(30,43);
				$pdf->SetFont('tahomab','',11);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620','ลำดับที่'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','เล่มที่/เลขที่'),0,0,"C");
				$pdf->Cell(35,7,iconv( 'UTF-8','TIS-620','วันเดือนปี'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','จำนวนเงิน'),0,0,"C");
				$pdf->Cell(50,7,iconv( 'UTF-8','TIS-620','หมายเหตุ'),0,0,"C");
				// Bill Table
				$pdf->SetFont('tahoma','',10);
				for ($i=1; $i <= $BillCnt ; $i++) { 
					$pdf->SetXY(30,(43+($i*7)));
					$sqlCre = "SELECT * FROM VatCredit WHERE ID='".$billList[($i-1)]."'";
					// echo $sqlCre." ".$i."<br>";
					$resultCre = mysqli_query($conn,$sqlCre);
					$rowCre = mysqli_fetch_assoc($resultCre);
					$yearID = (date("y",strtotime($rowCre['Date'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = "IV".$yearID."-";
					for ($j=0; $j < 4-strlen($rowCre['Code']); $j++) { 
						$id .= "0";
					}
					$id .= $rowCre['Code'];
					$Date = date('d/m/',strtotime($rowCre['Date'])).((date("Y",strtotime($rowCre['Date'])))+543);
					$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$i),0,0,"C");
					$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',$id),0,0,"C");
					$pdf->Cell(35,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"C");
					$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($rowCre['Total'], 2, '.', ',')),0,0,"R");
					$pdf->Cell(50,7,iconv( 'UTF-8','TIS-620',''),0,0,"C");
				}
				// Table Head
				$pdf->SetXY(30,110+$tmpLenght);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$BillCnt),0,0,"C");
				$pdf->SetXY(30,110+$tmpLenght);
				$pdf->Cell(28,7,iconv( 'UTF-8','TIS-620','  รวม         ฉบับ'),1,0,"L");
				$pdf->SetXY(85,110+$tmpLenght);
				$pdf->Cell(35,7,iconv( 'UTF-8','TIS-620','รวมเงิน'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($row['Total'], 2, '.', ',')),'TB',0,"R");
				$pdf->SetXY(120,111+$tmpLenght);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',''),'B',0,"R");
				// Signature
				$pdf->SetXY(30,131+$tmpLenght);
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','นัดชำระ....................................'),0,0,"L");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้รับวางบิล....................................'),0,0,"C");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้วางบิล....................................'),0,0,"R");
				// Output
				$pdf->Output();
				break;
			#-------------------------------------------------------------------------------------------------------------------------------------#

			//////////////////////////
			// Billing Invoice Page //
			//////////////////////////
			case 'billing_invoice':
				$sql = "SELECT * FROM BillingInvoice WHERE ID='".$_GET['id']."'";
				$result = mysqli_query($conn,$sql);
				$row = mysqli_fetch_assoc($result);
				$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
				$resultCus = mysqli_query($conn,$sqlCus);
				$rowCus = mysqli_fetch_assoc($resultCus);
				$invoiceList = explode("-", $row['InvoiceList']);

				$yearID = (date("y",strtotime($row['DateEnd'])))+43;
				if ($yearID > 100) {
					$yearID -= 100;
				}
				$id = '';
				for ($j=0; $j < 3-strlen($row['Code']); $j++) { 
					$id .= "0";
				}
				$id .= $row['Code'];

				$bookID = $yearID;
				$ID = $id;
				$Date = date('d/m/').(date('Y')+543);
				$CustomerName = $rowCus['Name'];
				$BillCnt = $row['BillCnt'];

				$tmpLenght = 0;
				$pdf = new FPDF();
				$pdf->SetAutoPageBreak(false);
				$pdf->AddPage('P','Letter');
				if ($BillCnt > 8) {
					$tmpLenght = 135;
				}
				$pdf->AddFont('tahoma');
				$pdf->AddFont('tahomab');
				// $pdf->Cell(1,0,"asdf");
				//ใบวางบิล
				$pdf->SetXY(93,18);
				$pdf->SetFont('tahomab','',15);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ใบวางบิล'),0,1,"L");
				// Default
				$pdf->SetFont('tahoma','',10);
				$pdf->SetXY(30,15);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','ไทยนิยมฮาร์ดแวร์'),0,1,"L");
				$pdf->SetX(30);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','856/11-12  ถ.เจตน์จำนง  ต.บางปลาสร้อย'),0,1,"L");
				$pdf->SetX(30);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','อ.เมืองชลบุรี  จ.ชลบุรี  20000       Tel.(038) 273507-8   Fax.(038) 273507'),0,1,"L");
				// เล่มบิล
				$pdf->SetXY(155,15);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เล่มที่'),0,0,"L");
				$pdf->SetX(170);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$bookID),0,1,"L");
				// เล่มที่
				$pdf->SetXY(155,22);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','เล่มที่'),0,0,"L");
				$pdf->SetX(170);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$ID),0,0,"L");
				// วันที่
				$pdf->SetXY(145,29);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620','วันที่'),0,0,"L");
				$pdf->SetX(155);
				$pdf->Cell(85,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"L");
				// Customer Name
				$pdf->SetXY(30,36);
				$pdf->SetFont('tahomab','',11);
				$pdf->Cell(125,7,iconv( 'UTF-8','TIS-620','  '.$CustomerName),1,1,"L");
				// Table Head
				$pdf->SetXY(30,43);
				$pdf->SetFont('tahomab','',11);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620','ลำดับที่'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','เล่มที่/เลขที่'),0,0,"C");
				$pdf->Cell(35,7,iconv( 'UTF-8','TIS-620','วันเดือนปี'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620','จำนวนเงิน'),0,0,"C");
				$pdf->Cell(50,7,iconv( 'UTF-8','TIS-620','หมายเหตุ'),0,0,"C");
				// Bill Table
				$pdf->SetFont('tahoma','',10);
				for ($i=1; $i <= $BillCnt ; $i++) { 
					$pdf->SetXY(30,(43+($i*7)));
					$sqlCre = "SELECT * FROM Invoice WHERE ID='".$invoiceList[($i-1)]."'";
					// echo $sqlCre." ".$i."<br>";
					$resultCre = mysqli_query($conn,$sqlCre);
					$rowCre = mysqli_fetch_assoc($resultCre);
					$yearID = (date("y",strtotime($rowCre['Date'])))+43;
					if ($yearID > 100) {
						$yearID -= 100;
					}
					$id = "TNY".$yearID."-";
					for ($j=0; $j < 4-strlen($rowCre['Code']); $j++) { 
						$id .= "0";
					}
					$id .= $rowCre['Code'];
					$Date = date('d/m/',strtotime($rowCre['Date'])).((date("Y",strtotime($rowCre['Date'])))+543);
					$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$i),0,0,"C");
					$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',$id),0,0,"C");
					$pdf->Cell(35,7,iconv( 'UTF-8','TIS-620',$Date),0,0,"C");
					$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($rowCre['Total'], 2, '.', ',')),0,0,"R");
					$pdf->Cell(50,7,iconv( 'UTF-8','TIS-620',''),0,0,"C");
				}
				// Table Head
				$pdf->SetXY(30,110+$tmpLenght);
				$pdf->SetFont('tahoma','',10);
				$pdf->Cell(25,7,iconv( 'UTF-8','TIS-620',$BillCnt),0,0,"C");
				$pdf->SetXY(30,110+$tmpLenght);
				$pdf->Cell(28,7,iconv( 'UTF-8','TIS-620','  รวม         ฉบับ'),1,0,"L");
				$pdf->SetXY(85,110+$tmpLenght);
				$pdf->Cell(35,7,iconv( 'UTF-8','TIS-620','รวมเงิน'),0,0,"C");
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',number_format($row['Total'], 2, '.', ',')),'TB',0,"R");
				$pdf->SetXY(120,111+$tmpLenght);
				$pdf->Cell(30,7,iconv( 'UTF-8','TIS-620',''),'B',0,"R");
				// Signature
				$pdf->SetXY(30,131+$tmpLenght);
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','นัดชำระ....................................'),0,0,"L");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้รับวางบิล....................................'),0,0,"C");
				$pdf->Cell(55,7,iconv( 'UTF-8','TIS-620','ผู้วางบิล....................................'),0,0,"R");
				// Output
				$pdf->Output();
				break;
			#-------------------------------------------------------------------------------------------------------------------------------------#
			
			default:
				# code...
				break;
		}
	}
	?>