<html>
<head>
	<?php
		session_start();
		include("connection.php");
    ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
	<title>ภาษีขาย</title>
</head>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ภาษีขาย</p></h2>
			<div class="margin-lr box center">
	            <span id="Pagination">
					<?php
						if ($_SESSION['orderlist'] != "sales_tax") {
							$_SESSION['orderlist'] = "sales_tax";
	            			$_SESSION['orderlistpage'] = 1;
						}
						include("pagination.php");
					?>
				</span>
	            <div class="table-responsive" id="OrderList">
	            	<?php
	            		include("orderlist.php");
	            	?>
	            </div>
	        </div>
		</div>
	</div>
</body>
</html>