<html>
<head>
	<?php
		include("connection.php");
		$space = "&nbsp;&nbsp;&nbsp;&nbsp;";
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>ดูบิลภาษีขาย</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ดูบิลภาษีขาย</p></h2>
			<div class="margin-lr box center">
				<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
				<div class="text-left margin-b">
					<div class="col">
						<?php
							echo '
								<table>
									<tr>
										<th width="80px">เลขที่บิล</th>
										<th width="80px">วันที่</th>
										<th width="50%">ชื่อลูกค้า</th>
										<th>ราคา</th>
										<th>Vat</th>
										<th>ราคารวม</th>
									</tr>';
							$sql = "SELECT * FROM (
									    (SELECT VatCredit.ID, VatCredit.Date, VatCredit.CustomerID, VatCredit.Vat, VatCredit.Total, VatCredit.Code, '2' AS ord, 'bill_credit' AS title FROM VatCredit)
									    UNION ALL
									    (SELECT VatCash.ID, VatCash.Date, VatCash.CustomerID, VatCash.Vat, VatCash.Total, VatCash.Code, '1' AS ord, 'bill_cash' AS title FROM VatCash)
									    UNION ALL
									    (SELECT TaxInvoice.ID, TaxInvoice.Date, '-1' AS CustomerID, TaxInvoice.Vat, TaxInvoice.Total, TaxInvoice.Code, '0' AS ord, 'tax_invoice' AS title FROM TaxInvoice)
									) results
									WHERE Date LIKE '%".$_GET['date']."%'
									ORDER BY Date ASC, ord ASC, Code ASC";
							#$sql = "SELECT * FROM (VatCredit) ORDER BY Date ASC";
							#$sql .= " LIMIT ".($_SESSION['orderlistpage']*20-20).",20";
							if($result = mysqli_query($conn,$sql)){
								$cnt = 1;
								$tmp = "";
								$arrayDate = array();
								$totalnovat = 0;
								$vat = 0;
								$total = 0;
								$first = 1;
								$allPrice = 0;
								$allVat = 0;
								$allTotal = 0;
								while($row = mysqli_fetch_assoc($result)){
									$yearID = (date("y",strtotime($row['Date'])))+43;
									if ($yearID > 100) {
										$yearID -= 100;
									}
									$sqlCus = "SELECT * FROM Customer WHERE ID='".$row['CustomerID']."'";
									$resCus = mysqli_query($conn, $sqlCus);
									$rowCus = mysqli_fetch_assoc($resCus);
									// Test
									// echo "</br>".$row['title'];
									$subtitle = "";
									switch ($row['title']) {
										case 'bill_credit':
											$subtitle = "credit";
											// Calcucate Vat & Total
											$totalnovat = round(($row['Total']/(100+$row['Vat'])*100),2);
											$vat = round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2);
											$total = $row['Total'];
											// ID
											$id = "IV".$yearID."-";
											for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
												$id .= "0";
											}
											$id .= $row['Code'];
											break;
										case 'bill_cash':
											$subtitle = "cash";
											// Calcucate Vat & Total
											$totalnovat = round(($row['Total']/(100+$row['Vat'])*100),2);
											$vat = round(($row['Total']/(100+$row['Vat'])*$row['Vat']),2);
											$total = $row['Total'];
											// ID
											$id = "CR".$yearID."-";
											for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
												$id .= "0";
											}
											$id .= $row['Code'];
											break;
										case 'tax_invoice':
											$subtitle = "taxinvoice";
											$rowCus['Name'] = "เงินสด";
											// Calcucate Vat & Total
											$percentVat = 7;
											$totalnovat = round(($row['Total']/(100+$percentVat)*100),2);
											$vat = round(($row['Total']/(100+$percentVat)*$percentVat),2);
											$total = $row['Total'];
											// ID
											$id = "CH".$yearID."-";
											for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
												$id .= "0";
											}
											$id .= $row['Code'];
											break;
										
										default:
											# code...
											break;
									}
									$href = "'".$row['title']."_view.php?".$subtitle."id=".$row['ID']."&back=sales_tax_view&date=".date("Y-m",strtotime($row['Date']))."'";
									$year = date("Y",strtotime($row['Date']))+543;

									echo '<tr class="view" onclick="document.location = '.$href.';">
										<td style="text-align:center;">'.$id.'</td>
										<td style="text-align:center;">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
										<td>'.$rowCus['Name'].'</td>
										<td style="text-align:right;">'.number_format($totalnovat, 2, '.', ',').$space.$space.'</td>
										<td style="text-align:right;">'.number_format($vat, 2, '.', ',').$space.$space.'</td>'
									."<td ";
									if ($cnt%2 == 1) {
										echo "style='background-color:#B4F082;";
									}else{
										echo "style='background-color:#99E857;";
									}
									echo "text-align:right;'>".number_format($total, 2, '.', ',').$space.$space."</td>"
										."</tr>";
									$cnt++;
									$_SESSION['ID'] = $row['ID'];
									$allPrice += $totalnovat;
									$allVat += $vat;
									$allTotal += $total;
								}
								$cnt++;
								$_SESSION['ID'] = $row['ID'];
							}
							echo '<tr class="view" onclick="document.location = '.$href.';">
								<td colspan="3" style="text-align:center;">รวม</td>
								<td style="text-align:right;">'.number_format($allPrice, 2, '.', ',').$space.$space.'</td>
								<td style="text-align:right;">'.number_format($allVat, 2, '.', ',').$space.$space.'</td>'
							."<td ";
							if ($cnt%2 == 1) {
								echo "style='background-color:#B4F082;";
							}else{
								echo "style='background-color:#99E857;";
							}
							echo "text-align:right;'>".number_format($allTotal, 2, '.', ',').$space.$space."</td>"
								."</tr>";
							echo	'</table>';
						?>
					</div>
				</div>
				<a href='sales_tax.php' class='btn'>กลับ</a>
			</div>
		</div>
	</div>
</body>
</html>