<html>
<head>
	<?php
		session_start();
		include("connection.php");
    ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
	<title>ระบบ</title>
</head>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ระบบ</p></h2>
			<div class="margin-lr box center">
	            <div class="table-responsive">
	            	<span id="OrderList">
						<a class="btn smooth" target="blank" href="backup_database.php">Backup</a><br>
						จะได้เป็น file .sql
	            	</span>
	            </div>
	        </div>
		</div>
	</div>
</body>
</html>