<html>
<head>
	<?php
		include("connection.php");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>เพิ่มบิลใบกำกับภาษี</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">เพิ่มบิลใบกำกับภาษี</p></h2>
			<div class="margin-lr box center">
				<form action="tax_invoice_add_process.php" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								$year = date("Y")+543;
								// ID
								$sql = "SELECT * FROM TaxInvoice";
								if ($result = mysqli_query($conn, $sql)) {
									$max_code = 0;
									while ($rowTaxInvoice = mysqli_fetch_assoc($result)) {
										if (date("Y", strtotime($rowTaxInvoice['Date'])) == date("Y") && $max_code < $rowTaxInvoice['Code']) {
											$max_code = $rowTaxInvoice['Code'];
										}
									}
								}
								$next_code = $max_code+1;
								$yearID = date("y")+43;
								if ($yearID > 100) {
									$yearID -= 100;
								}
								$id = "IV".$yearID."-";
								for ($i=0; $i < 4-strlen($next_code); $i++) { 
									$id .= "0";
								}
								$id .= $next_code;
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td>
													เลขที่บิล
												</td>
												<td>
													'.$id.'<input name="TaxInvoiceCode" type="hidden" value="'.($next_code).'"/>
												</td>
											</tr>
											<tr>
												<td style="width:90px">วันที่</td>
												<td colspan="2"><input name="TaxInvoiceDate" type="text" value="'.date("d-m-").$year.'"/></td>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px">เงินสด</td>
											</tr>
										</table>
										<table class="noborder margin-b list" style="width:auto">
											<tr>
												<td colspan="5"><b style="font-size:1.4em;">รายการสินค้า</b></td>
											</tr>
											<tr>
												<td class="text-cen" style="height:50px">เลขที่</td>
												<td class="text-cen">ชื่อ</td>
												<td class="text-cen">จำนวน</td>
												<td class="text-cen">หน่วย</td>
												<td class="text-cen">ราคาต่อหน่วย</td>
												<td style="width: 100px" class="text-cen">รวม</td>
											</tr>
											<tr>
												<td>1</td>
												<td><input class="name" name="ProductName1" type="text" /></td>
												<td><input name="ProductQuantity1" id="ProductQuantity1" type="text" value="0" onkeyup="CalPrice(\'1\')"/></td>
												<td><input name="ProductUnit1" type="text" /></td>
												<td><input name="ProductPriceEach1" id="ProductPriceEach1" type="text" value="0" onkeyup="CalPrice(\'1\')"/></td>
												<td class="text-right">
													<span id="ProductTotalText1">0.00</span>
													<input type="hidden" id="ProductTotal1" name="ProductTotal1" value="0" />
												</td>
											</tr>
											<tr>
												<td class="text-cen" colspan="5"><b style="font-size:1.2em;">รวม</b></td>
												<td class="text-right">
													<span id="ProductAllTotalText">0.00</span>
													<input type="hidden" id="ProductAllTotal" name="ProductAllTotal" value="0" />
												</td>
											</tr>
											<tr></tr>
											<tr></tr>
										</table>
										<table class="noborder margin-b" style="width:auto">
											<tr>
												<td width="220px">
												</td>
												<td>
													<a class="btn smooth del-etc-row float-r">-</a>
													<div class="float-r">&nbsp;</div>
													<a class="btn smooth add-etc-row float-r">+</a>
												</td>
											</tr>
										</table>';
							?>
						</div>
					</div>
					
					<?php $_SESSION['tax_invoice_add'] = strtotime(date('Y-m-d H:i:s')); ?>
					<input type="hidden" name="save_token" value="<?=$_SESSION['tax_invoice_add']?>">
					<input class="btn smooth" type="submit" value="เพิ่มบิล"></a>
					<a class="btn smooth" href="tax_invoice.php">กลับ</a>
					<input type="hidden" id="itemSize" name="itemSize" value="1" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>
