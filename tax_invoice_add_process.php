<?php
	include("connection.php");

	if( $_SESSION['tax_invoice_add'] == $_POST['save_token'])
	{
		$_SESSION['tax_invoice_add'] = '';
		$lotID = 0;
		$sql = "SELECT * FROM Product";
		$result = mysqli_query($conn, $sql);
		while ($row = mysqli_fetch_assoc($result)) {
			if ($row['LotID'] > $lotID) {
				$lotID = $row['LotID'];
			}
		}
		$lotID++;
	
		$date = explode('-', $_POST['TaxInvoiceDate']);
		$sql = "INSERT INTO TaxInvoice VALUE('', '".($date[2]-543)."-".$date[1]."-".$date[0]	//1,2
			."', '".$lotID								//3
			."', '7"									//4
			."', '".$_POST['ProductAllTotal']			//5
			."', '".$_POST['TaxInvoiceCode']			//5
			."')";
		for ($i=1; $i <= $_POST['itemSize']; $i++) {
			$productName = str_replace('"','\"',$_POST['ProductName'.$i]);
			$productName = str_replace("'","\'",$productName);
			$sqlTmp = "INSERT INTO Product VALUE('', '".$productName
				."', '".$_POST['ProductQuantity'.$i]
				."', '".$_POST['ProductUnit'.$i]
				."', '".$_POST['ProductPriceEach'.$i]
				."', '".$lotID
				."')";
			mysqli_query($conn,$sqlTmp);
		}
		if(mysqli_query($conn,$sql)){
			$_SESSION['Status'] = "Add Success.";
			header("location:tax_invoice.php");
		}else{
			$_SESSION['Status'] = "Add Error.";
			if (isset($_SERVER["HTTP_REFERER"])) {
						header("Location: " . $_SERVER["HTTP_REFERER"]);
				}
		}
		mysqli_close($conn);
	}
?>
