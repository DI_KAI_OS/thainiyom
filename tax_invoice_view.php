<html>
<head>
	<?php
		include("connection.php");
		$space = "&nbsp;&nbsp;&nbsp;&nbsp;";
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<title>ดูบิลใบกำกับภาษี</title>
<body>
	<div class="menu-theme shadow">
		<?php include("menu.php"); ?>
	</div>
	<div class="main">
		<div class="col">
			<h2><p class="margin-tb text-cen">ดูบิลใบกำกับภาษี</p></h2>
			<div class="margin-lr box center">
				<form action="bill_cash_add_process.php" method="post" enctype="multipart/form-data">
					<?php echo $_SESSION['Status']; unset($_SESSION['Status']);?>
					<div class="text-left">
						<div class="col">
							<?php
								// Tax Invoice
								$sql = "SELECT * FROM TaxInvoice WHERE ID='".$_GET['taxinvoiceid']."'";
								$result = mysqli_query($conn, $sql);
								$row = mysqli_fetch_assoc($result);
								// Change to Buddha Year
								$year = date("Y",strtotime($row['Date']))+543;// ID
								$yearID = (date("y",strtotime($row['Date'])))+43;
								if ($yearID > 100) {
									$yearID -= 100;
								}
								$id = "CH".$yearID."-";
								for ($i=0; $i < 4-strlen($row['Code']); $i++) { 
									$id .= "0";
								}
								$id .= $row['Code'];
								echo '	<table class="noborder margin-b" style="width:auto">
											<tr>
												<td style="width:90px">เลขที่บิล</td>
												<td colspan="2">'.$id.'</td>
											</tr>
											<tr>
												<td style="width:90px">วันที่</td>
												<td colspan="2">'.date("d-m-",strtotime($row['Date'])).$year.'</td>
											</tr>
											<tr>
												<td><b style="font-size:1.4em;">ลูกค้า</b></td>
												<td style="width:130px"> เงินสด </td>
											</tr>
										</table>
										<table class="noborder margin-b list">
											<tr>
												<td colspan="5"><b style="font-size:1.4em;">รายการสินค้า</b></td>
											</tr>
											<tr>
												<td class="text-cen" style="height:50px"><b> เลขที่ </b></td>
												<td class="text-cen"><b> ชื่อ </b></td>
												<td class="text-cen"><b> จำนวน </b></td>
												<td class="text-cen"><b> หน่วย </b></td>
												<td class="text-cen"><b> ราคาต่อหน่วย </b></td>
												<td class="text-cen"><b> รวม </b></td>
											</tr>';
								$sql = "SELECT * FROM Product WHERE LotID='".$row['LotID']."'";
								if ($result = mysqli_query($conn, $sql)) {
									$cnt = 1;
									$total = 0;
									while ($rowPro = mysqli_fetch_assoc($result)) {
										echo '<tr>
												<td class="text-cen" style="width:5%;">'.$cnt.'</td>
												<td class="text-left" style="width:45%;"> '.$space.$rowPro['Name'].$space.'</td>
												<td class="text-cen" style="width:10%;"> '.$space.$rowPro['Quantity'].$space.'</td>
												<td class="text-cen" style="width:10%;"> '.$space.$rowPro['Unit'].$space.'</td>
												<td class="text-right" style="width:15%;"> '.$space.number_format($rowPro['PriceEach'], 2, '.', ',').$space.'</td>
												<td class="text-right" style="width:15%;"> '.$space.number_format(($rowPro['PriceEach']*$rowPro['Quantity']), 2, '.', ',').$space.'</td>
											</tr>
											';
										$total += $rowPro['PriceEach']*$rowPro['Quantity'];
										$cnt++;
									}
								}
								echo		'<tr>
												<td class="text-cen" colspan="5"><b style="font-size:1.5em;">รวมทั้งสิ้น</b></td>
												<td class="text-right">'.$space.number_format($total, 2, '.', ',').$space.'</td>
											</tr>
										</table>';
							?>
						</div>
					</div>
					<a href='print.php?type=tax_invoice&id=<?php echo $_GET['taxinvoiceid']; ?>' class='btn smooth' target="_blank">ดูตัวอย่างก่อนพิมพ์</a>
		            <?php
						if (isset($_GET['back'])) {
							if (isset($_GET['date'])) {
								echo "<a class='btn smooth' href='tax_invoice_edit.php?taxinvoiceid=".$_GET['taxinvoiceid']."&back=".$_GET['back']."&date=".$_GET['date']."'>แก้ไข</a> ";
								echo "<a href='".$_GET['back'].".php?date=".$_GET['date']."' class='btn'>กลับ</a>";
							}else{
								echo "<a class='btn smooth' href='tax_invoice_edit.php?taxinvoiceid=".$_GET['taxinvoiceid']."&back=".$_GET['back']."'>แก้ไข</a> ";
								echo "<a href='".$_GET['back'].".php' class='btn'>กลับ</a>";
							}
						}else{
							echo "<a class='btn smooth' href='tax_invoice_edit.php?taxinvoiceid=".$_GET['taxinvoiceid']."'>แก้ไข</a> ";
							echo "<a href='tax_invoice.php' class='btn'>กลับ</a>";
						}
		            ?>
				</form>
			</div>
		</div>
	</div>
</body>
</html>